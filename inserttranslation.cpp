#include "inserttranslation.h"
#include "ui_inserttranslation.h"

#include "config.h"

InsertTranslation::InsertTranslation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InsertTranslation)
{
    ui->setupUi(this);

    insertFlag=false;

    modeMulti=modeFirst=false;

    ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setText("Insert");
    ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(false);

    std::string winTitle="Insert translation";
    winTitle.append(" @ ");
    winTitle.append(PROJECT_NAME);


    setWindowTitle(QString::fromStdString(winTitle));

    setWindowIcon(QIcon(":/icon.ico"));

    home_insert_path=getenv("USERPROFILE");
    home_insert_path.append("/Documents");
}

InsertTranslation::~InsertTranslation()
{
    delete ui;
}
QString InsertTranslation::getOpen(QString title,QString filter){
    QString myFilter=filter;

    QString myAllFilters=myFilter;
    myAllFilters.append(";; All files (*.*)");

    QString selFilter = filter;
    QString path = QFileDialog::getOpenFileName(
            this,
            title,
            home_insert_path,
            myAllFilters,
            &selFilter
    );

    home_insert_path=path;
    return path;
        }

void InsertTranslation::on_btnFilePatch_clicked()
{
    QString filter("Game file (*.bin *.ext *.dat *.incs *.loctext)");
    QString title("Choose raw file");

    QString openRawToPatch=getOpen(title,filter);

    if(openRawToPatch.isNull()){
        return;
    }

    ui->txtFilePatch->setPlainText(openRawToPatch);
}


void InsertTranslation::on_btnPointersTranslation_clicked()
{
    QString filter("Pointers HexDump (*.txt)");
    QString title("Choose translation pointers");

    QString openTranslationPointersHex=getOpen(title,filter);

    if(openTranslationPointersHex.isNull()){
        return;
    }

    ui->txtPointersTranslation->setPlainText(openTranslationPointersHex);
}


void InsertTranslation::on_btnTextTranslation_clicked()
{
    QString filter("Text HexDump (*.txt)");
    QString title("Choose translation text");

    QString openTranslationTextHex=getOpen(title,filter);

    if(openTranslationTextHex.isNull()){
        return;
    }

    ui->txtTextTranslation->setPlainText(openTranslationTextHex);
}

void InsertTranslation::checkInputs(){
    if(!ui->txtFilePatch->toPlainText().isEmpty()&&
            !ui->txtTextOriginalSection->toPlainText().isEmpty()&&
            !ui->txtTextTranslation->toPlainText().isEmpty()&&
            !ui->txtPointersOriginalSection->toPlainText().isEmpty()&&
            !ui->txtPointersTranslation->toPlainText().isEmpty()){
        if(modeFirst||modeMulti){
            if(!ui->txtPointersOriginal->toPlainText().isEmpty()){
                ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(true);
            }
            else{
                ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(false);
            }
        }
        else if(modeAll){
            ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(true);
        }
        else{
            ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(false);
        }
    }
    else{
        ui->btnInsertTranslation->button(QDialogButtonBox::Save)->setEnabled(false);
    }
}

void InsertTranslation::on_txtFilePatch_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_txtPointersTranslation_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_txtTextTranslation_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_btnInsertTranslation_accepted()
{
    insertFlag=true;
    filePatchPath=ui->txtFilePatch->toPlainText();
    pointersOriginalSectionPath=ui->txtPointersOriginalSection->toPlainText();
    pointersTranslationPath=ui->txtPointersTranslation->toPlainText();
    textOriginalSectionPath=ui->txtTextOriginalSection->toPlainText();
    textTranslationPath=ui->txtTextTranslation->toPlainText();

    pointersOriginal=ui->txtPointersOriginal->toPlainText();

}

void InsertTranslation::on_InsertTranslation_rejected()
{
    insertFlag=false;
}

void InsertTranslation::on_btnInsertTranslation_rejected()
{
    insertFlag=false;
}

void InsertTranslation::on_chkMulti_toggled(bool checked)
{
    modeMulti=checked;
    modeFirst=false;
    modeAll=false;
    modeUnordered=false;

    ui->chkFirst->blockSignals(true);
    ui->chkFirst->setChecked(false);
    ui->chkFirst->blockSignals(false);

    ui->chkAll->blockSignals(true);
    ui->chkAll->setChecked(false);
    ui->chkAll->blockSignals(false);

    ui->chkUnordered->blockSignals(true);
    ui->chkUnordered->setChecked(false);
    ui->chkUnordered->blockSignals(false);

    ui->btnPointersOriginal->setEnabled(checked);
    ui->txtPointersOriginal->setEnabled(checked);

    checkInputs();
}

void InsertTranslation::on_chkFirst_toggled(bool checked)
{
    modeFirst=checked;
    modeMulti=false;
    modeAll=false;
    modeUnordered=false;

    ui->chkMulti->blockSignals(true);
    ui->chkMulti->setChecked(false);
    ui->chkMulti->blockSignals(false);

    ui->chkAll->blockSignals(true);
    ui->chkAll->setChecked(false);
    ui->chkAll->blockSignals(false);

    ui->chkUnordered->blockSignals(true);
    ui->chkUnordered->setChecked(false);
    ui->chkUnordered->blockSignals(false);

    ui->btnPointersOriginal->setEnabled(checked);
    ui->txtPointersOriginal->setEnabled(checked);

    checkInputs();
}


void InsertTranslation::on_chkAll_toggled(bool checked)
{
    modeAll=checked;
    modeMulti=false;
    modeFirst=false;
    modeUnordered=false;

    ui->chkMulti->blockSignals(true);
    ui->chkMulti->setChecked(false);
    ui->chkMulti->blockSignals(false);

    ui->chkFirst->blockSignals(true);
    ui->chkFirst->setChecked(false);
    ui->chkFirst->blockSignals(false);

    ui->chkUnordered->blockSignals(true);
    ui->chkUnordered->setChecked(false);
    ui->chkUnordered->blockSignals(false);

    ui->btnPointersOriginal->setEnabled(false);
    ui->txtPointersOriginal->setEnabled(false);

    checkInputs();
}


void InsertTranslation::on_btnTextOriginalSection_clicked()
{
    QString filter("Text HexDump (*.txt *.dat *.incs *.loctext *.bin)");
    QString title("Choose original text");

    QString openOriginalTextHex=getOpen(title,filter);

    if(openOriginalTextHex.isNull()){
        return;
    }

    ui->txtTextOriginalSection->setPlainText(openOriginalTextHex);
}

void InsertTranslation::on_btnPointersOriginalSection_clicked()
{
    QString filter("Pointers HexDump (*.txt *.dat *.incs *.loctext *.bin)");
    QString title("Choose original pointers");

    QString openOriginalPointersSectionHex=getOpen(title,filter);

    if(openOriginalPointersSectionHex.isNull()){
        return;
    }

    ui->txtPointersOriginalSection->setPlainText(openOriginalPointersSectionHex);
}

void InsertTranslation::on_btnPointersOriginal_clicked()
{
    QString filter("Pointers HexDump (*.txt)");
    QString title("Choose original pointers from text");

    QString openPointersOriginal=getOpen(title,filter);

    if(openPointersOriginal.isNull()){
        return;
    }

    ui->txtPointersOriginal->setPlainText(openPointersOriginal);
}

void InsertTranslation::on_txtPointersOriginalSection_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_txtTextOriginalSection_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_txtPointersOriginal_textChanged()
{
    checkInputs();
}

void InsertTranslation::on_chkUnordered_toggled(bool checked)
{
    modeUnordered=checked;
    modeAll=false;
    modeMulti=false;
    modeFirst=false;

    ui->chkAll->blockSignals(true);
    ui->chkAll->setChecked(false);
    ui->chkAll->blockSignals(false);

    ui->chkMulti->blockSignals(true);
    ui->chkMulti->setChecked(false);
    ui->chkMulti->blockSignals(false);

    ui->chkFirst->blockSignals(true);
    ui->chkFirst->setChecked(false);
    ui->chkFirst->blockSignals(false);

    ui->btnPointersOriginal->setEnabled(false);
    ui->txtPointersOriginal->setEnabled(false);

    checkInputs();
}
