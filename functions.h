#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "vars.h"
#include "mainwindow.h"
#include "inih/INIReader.h"

vector <string> MainWindow::split(const string& input, const string& regex) {
    // passing -1 as the submatch index parameter performs splitting
    std::regex re(regex);
    sregex_token_iterator
            first{input.begin(), input.end(), re, -1},
    last;
    return {first, last};
}

void MainWindow::writeFile(std::ostream &fileToWrite,QString stringToWrite){
    fileToWrite << stringToWrite.toStdString();
}

QString MainWindow::getLEString(string stringHexIn,bool reverse,bool pointers,int byteSize){
    string little_endian_h="";
    int sizestr;
    if(pointers){
        sizestr=byteSize*2;
    }else{
        sizestr=byteSize*2;
    }
    vector <string> entries;
    if (!pointers){
        string element="";
        for (unsigned int i = 0; i < stringHexIn.length(); i += 2) {
            string thisChar=stringHexIn.substr(i,2);
            if(thisChar==entry_delimiter.toStdString()){
                entries.push_back(element);
                element="";
                continue;
            }
            element+=thisChar;
        }
    }
    else{
        for (unsigned int i = 0; i < stringHexIn.length(); i += sizestr) {
            entries.push_back(stringHexIn.substr(i, sizestr));
        }
    }

    for (unsigned int i=0;i<(unsigned int)entries.size();i++) {
        string thisElement=entries.at(i);

            QStringList bytes;
            for(unsigned int j=0;j<(unsigned int)thisElement.length();j+=2){
                bytes.push_back(QString::fromStdString(thisElement.substr(j,2)));
            }
            if(!pointers){
                bytes.push_back(entry_delimiter_custom);
            }

            if(reverse){
                for(int j=bytes.size()-1;j>=0;j--){
                    little_endian_h.append(bytes.at(j).toStdString());
                }
            }
            else{
                for(unsigned int j=0;j<(unsigned int) bytes.size();j++){
                    little_endian_h.append(bytes.at(j).toStdString());
                }
            }
//        }
    }
    //To Uppercase
    transform (little_endian_h.begin(), little_endian_h.end(),
               little_endian_h.begin(), ::toupper);

    return QString::fromStdString(little_endian_h);
}

void MainWindow::create_temp_dir(){

    QDir t(temp_folder_path);
    if(QDir(t.absolutePath()).exists()){
        QDir(t.absolutePath()).removeRecursively();
    }
    QDir().mkdir(t.absolutePath());
}

void MainWindow::setNewProject(QFileInfo info){
    //project_path=info.filePath();
    raw_script_dir.setPath(info.filePath());
    //project_filename=info.fileName();
    //project_suffix=info.suffix();
    project_base_name=info.baseName();
    project_root_absolute_path=info.absolutePath();
}

void MainWindow::setProject(QFileInfo info){
    //project_path=info.filePath();
    //project_dir.setPath(info.filePath());
    project_filename=info.fileName();
    //project_suffix=info.suffix();
    project_base_name=info.baseName();
    project_root_absolute_path=info.absolutePath();
}

int MainWindow::populate_create(){
    script_entries_original.clear();
    script_entries_translation.clear();
    vector <string>  this_entry_original;
    //thisTextTranslation;
    ui->lstEntries->clear();
    //QByteArray pText = project_dir.path().toLocal8Bit().data();
//    open_script_full_path = pText.data();
    //QByteArray pTable = table_path.toLocal8Bit();
    //const char *myTable = pTable.data();

    //QDir dir(QString::fromStdString(project_dir.path().toLocal8Bit().data()));
    QFileInfo info(raw_script_path);
    setNewProject(info);
    QDir temp_dir(temp_folder_path+QDir::separator()+project_base_name+temp_copy_suffix+project_suffix);
    // Revert little endian format (or not) and replace entry end characters
    ifstream original_text_section_h_stream {raw_script_dir.path().toStdString() };
    string original_text_section {std::istreambuf_iterator<char>(original_text_section_h_stream), std::istreambuf_iterator<char>() };
    //Reverse little endian in text
    QString LE_hex_str=getLEString(original_text_section,reverse_flag,false,char_size/2);
    //Save in temporal file
    ofstream LE_hex_stream;
    LE_hex_stream.open(temp_dir.path().toStdString());
    writeFile(LE_hex_stream,LE_hex_str);
    LE_hex_stream.close();
    //Read table file
    ifstream table_stream {table_path.toStdString() };
    vector<string> table;
    string str;
    while (getline(table_stream, str))
    {
        table.push_back(str);
    }
    //Prepare table code and character listss
    if(prepareTables(table)){
        return 1;
    }
    //Parse hex string
    unsigned int letters=0;
    for (unsigned int i=0;i<(unsigned int) LE_hex_str.length();i+=letters) {
        letters=0;
        QString phrase;
        for (unsigned int j=i;j<(unsigned int)LE_hex_str.length();) {
            //Get first code letter
            QString this_letter_code;
            //First try to get a 3-byte code
            try {
                this_letter_code=QString::fromStdString(LE_hex_str.toStdString().substr(j,6));
            } catch (exception ex) {
//                return 1;
                cout <<"Not enough characters"<<endl;
            }

            bool found=false;
            letters+=6;

            if (this_letter_code.size()==4) {
                goto four;
            }
            else if (this_letter_code.size()==2){
                goto two;
            }
            for (unsigned int k=0;k<table_codes.size();k++) {
                //Append current character matching character code from table
                if(this_letter_code.toStdString()==table_codes.at(k)){
                    phrase.append(QString::fromStdString(table_characters.at(k)));
                    found=true;
                    break;
                }
            }
            four:
            if(!found){
                //First try to get a 2-byte code
                try {
                    this_letter_code=QString::fromStdString(LE_hex_str.toStdString().substr(j,4));
                } catch (exception ex) {
//                    return 1;
                    cout <<"Not enough characters"<<endl;
                }

//                bool found=false;
                letters-=2;

                if (this_letter_code.size()==2) {
                    goto two;
                }
                for (unsigned int k=0;k<table_codes.size();k++) {
                    //Append current character matching character code from table
                    if(this_letter_code.toStdString()==table_codes.at(k)){
                        phrase.append(QString::fromStdString(table_characters.at(k)));
                        found=true;
                        break;
                    }
                }
            }
            two:
            if(!found){

                //First try to get a 1-byte code
                try {
                    this_letter_code=QString::fromStdString(LE_hex_str.toStdString().substr(j,2));
                } catch (exception ex) {
//                    return 1;
                    cout <<"Not enough characters"<<endl;
                }
                // If delimiter code, push it
                if(this_letter_code==entry_delimiter_custom){
                    letters-=2;
                    this_entry_original.push_back(phrase.toStdString());
                    break;
                }

                //If NO delimiter, push character
                letters-=2;
                for (unsigned int k=0;k<table_codes.size();k++) {
                    //Append current character matching character code from table
                    if(this_letter_code.toStdString()==table_codes.at(k)){
                        phrase.append(QString::fromStdString(table_characters.at(k)));
                        break;
                    }
                }
            }
            j+=this_letter_code.size();
        }
    }

    //Separate dialogue phrases
    for (unsigned int i=0;i<this_entry_original.size();++i) {
        //Replace escaped line breaks with proper line break
        findAndReplace(this_entry_original.at(i),ch_line_break_escape,ch_line_break,false,false);
        //Split lines
        QStringList lines=splitByDelimiter(this_entry_original.at(i),ch_line_break.toStdString());
        //Push lines list
        script_entries_original.push_back(lines);
    }

    //Set empty translation
    for (unsigned int i=0;i<this_entry_original.size();i++) {
        //Empty initial translation entries
        QStringList v;
        script_entries_translation.push_back(v);
    }

    //Set false default font width and extra space
    for (unsigned int i=0;i<script_entries_original.size();i++) {
        script_entries_font_width.push_back(false);
        script_entries_extra_space.push_back(false);
    }

    setWindowTit(project_base_name);

    //If not zero (true), terminate
    if(populateGUI(script_entries_original,script_entries_translation,script_entries_font_width,script_entries_extra_space)){
        return -1;
    }

    return 0;
}

QStringList MainWindow::splitByDelimiter(string stringToBeSplitted, string delimeter)
{
    QStringList splittedString;
    unsigned int startIndex = 0;
    unsigned int  endIndex = 0;
    while( (endIndex = stringToBeSplitted.find(delimeter, startIndex)) < (unsigned int)stringToBeSplitted.size() )
    {
        string val = stringToBeSplitted.substr(startIndex, endIndex - startIndex);
        splittedString.push_back(QString::fromStdString(val));
        startIndex = endIndex + delimeter.size();
    }
    if(startIndex < (unsigned int)stringToBeSplitted.size())
    {
        string val = stringToBeSplitted.substr(startIndex);
        splittedString.push_back(QString::fromStdString(val));
    }
    return splittedString;
}

wstring MainWindow::readFile(const char* filename)
{
    wifstream wif(filename);
    wif.imbue(std::locale(std::locale(), new codecvt_utf8<wchar_t>));
    wstringstream wss;
    wss << wif.rdbuf();
    wif.close();
    return wss.str();
}

void MainWindow::setWindowTit(QString basename){
    setWindowTitle(basename+w_title);
}

void MainWindow::list_set_color(QListWidgetItem *item,QStringList text){
    QColor c;
    if (text.size()) {
        c.setRgb(0,255,0);
        item->setBackground(c);
    }
}

void MainWindow::populateList(){

    ui->lstEntries->clear();
    untranslated=0;
    for (unsigned int i=0;i<script_entries_original.size();i++) {
        string row=to_string(i+1);
        row.append("  ");
        //If translation exists, set first line to list
        if(script_entries_translation.at(i).size()){
            try {
                row.append(script_entries_translation.at(i).at(0).toStdString());
            } catch (exception e) {

            }
        }
        //If NO translation exists, set first original line to list
        else if(script_entries_original.at(i).size()){
            try {
                row.append(script_entries_original.at(i).at(0).toStdString());

                //Left untranslated increase only when original text exist
                if(script_entries_original.at(i).at(0).toStdString().size()){
                    untranslated++;
                }
            } catch (exception e) {
            }
        }

        QString w=QString::fromStdString(row);
        ui->lstEntries->addItem(w);

        try {
            list_set_color(ui->lstEntries->item(i),script_entries_translation.at(i));
        } catch (exception ex) {

        }
    }


    //Update untranslated dialogues number
    QString s("Left: "+QString::fromStdString(to_string(untranslated)));
    ui->lblLeft->setText(s);
}

int MainWindow::populateGUI( vector <QStringList> eventOriginaLines,vector <QStringList> eventTranslationLines,vector<bool> entriesWidth, vector<bool> entriesExtraSpace){

    //Populate left dialogues list
    populateList();
    //Set current row to first row
    current_row_index=0;
    ui->lstEntries->setCurrentRow( 0 );
    //Parse the dialogue lines to show them
    QString o,t;
    o=prepareLinesToShow(eventOriginaLines,0);
    t=prepareLinesToShow(eventTranslationLines,0);
    //Update the GUI elements
    ui->txtOriginal->setText(o);
    ui->txtTranslation->setText(t);
    try {
        ui->cbFullWidth->setCurrentIndex(entriesWidth.at(0));
        ui->chkExtraSpace->setChecked(entriesExtraSpace.at(0));
        //Set png fonts preview
        setPreview(eventTranslationLines.at(0));
    } catch (exception ex) {
        return -1;
    }
    ui->chkFullWidth->setChecked(false);
    ui->cbFullWidth->setEnabled(false);
    ui->chkFullWidth->setEnabled(true);
    ui->btnBlue->setEnabled(true);
    ui->chkExtraSpace->setEnabled(true);
    ui->txtTranslation->setEnabled(true);
    //Set no changes in translation, initially
    setTranslationChanged(false);
    //Parse blue color words inside GUI element
    //set_color_words(o,ui->txtOriginal);
    set_color_words(t,ui->txtTranslation);

    return 0;
}

void MainWindow::set_text_cursor(QTextEdit *text_edit)
{
    QTextCursor cursor1= text_edit->textCursor();
    cursor1.atEnd();
    text_edit->setTextCursor(cursor1);
}

void MainWindow::set_text_cursor_start(QTextEdit *text_edit){
    QTextCursor cursor1= text_edit->textCursor();
    cursor1.setPosition(0);
    text_edit->setTextCursor(cursor1);
}

int  MainWindow::prepareTables(vector<string> table){

    table_codes.clear();
    table_characters.clear();

    for (unsigned int i=0;i<table.size();i++) {
        string thisCharacter=table.at(i);
        string code;
        QString character;
        //Split codes and characters
        vector <string> codeAndCharacter= split(thisCharacter, ch_equal_escape.toStdString());
        try {
            code=codeAndCharacter.at(0);
            character=QString::fromStdString(codeAndCharacter.at(1));
        } catch (exception &e) {
            QMessageBox msgBox;
            QString msg("Wrong table!");
            msgBox.setText(msg);
            msgBox.exec();
            cout <<msg.toStdString()<<endl;
            return 1;
        }

        //To uppercase codes
        transform (code.begin(), code.end(), code.begin(), ::toupper);
        table_codes.push_back(code);
        table_characters.push_back(character.toStdString());
    }
    table_loaded=true;
    return 0;
}


void MainWindow::updateLinesNumber(){

    //Read GUI element lines
    QStringList lines= splitByLines(ui->txtTranslation->document());
    unsigned int lines_number=0;

    if(lines.size()){
        if(lines.at(0).isEmpty()){
            lines_number=0;
        }
        else{
            lines_number=lines.size();
        }
    }

    ui->lineCount->setText("Lines: "+QString::fromStdString(to_string(lines_number)));
}

QString MainWindow::prepareLinesToShow(vector <QStringList> eventEntries,int index){
    QString l;
    if(!eventEntries.size()){
        return l;
    }
    for (unsigned int i=0;i<(unsigned int)eventEntries.at(index).size();i++) {
        //Not last line, append a line break at the end of the line
        if(!(i==(unsigned int)eventEntries.at(index).size()-1)){
            l.append(eventEntries.at(index).at(i)+ch_line_break);
        }
        else{
            l.append(eventEntries.at(index).at(i));
        }
    }
    return l;
}

void MainWindow::set_color_words(QString t,QTextEdit *text_edit){

    unsigned int start=0,end=0;
    for (unsigned int i=0;i<(unsigned)t.size();i++) {
        //If open delimiter, move two positions forward; set start position
        if(t.toStdString().substr(i,2)==sc_blue_words_delimiters.at(0).toStdString()){
            start=i+2;
            //If a open delimiter found, go to next iteration
            continue;
        }
        //If close position, set end position
        else if(t.toStdString().substr(i,2)==sc_blue_words_delimiters.at(1).toStdString()){
            end=i;
            //If a close delimiter found, proceed to update the text
        }
        else{
            //If no close or end delimiters found, go to next iteration
            continue;
        }

        QTextCursor cursor= text_edit->textCursor();
        cursor.setPosition(start);
        cursor.setPosition(end,QTextCursor::KeepAnchor);
        text_edit->setTextCursor(cursor);

        QString sel=text_edit->textCursor().selectedText();

        QString spanPre="<span style=\"color:blue\">";
        QString spanPost="</span>";
        text_edit->insertHtml(spanPre+sel+spanPost);
        //Set cursor at start position to prevent wrong coloring
        set_text_cursor_start(text_edit);

    }
    setTranslationChanged(false);
}


void MainWindow::updateElements(){

    ui->chkExtraSpace->setChecked(false);
    //Parse the dialogue lines to show them
    QString o,t;

    o=prepareLinesToShow(script_entries_original,new_row_index);

    t=prepareLinesToShow(script_entries_translation,new_row_index);

    bool selectedFontFullWidth=script_entries_font_width.at(new_row_index);

    bool selectedExtraSpace=script_entries_extra_space.at(new_row_index);
    //Update the GUI elements
    ui->txtOriginal->setText(o);
    ui->txtTranslation->setPlainText(t);
    ui->cbFullWidth->setCurrentIndex(selectedFontFullWidth);
    ui->chkExtraSpace->setChecked(selectedExtraSpace);
    ui->lstEntries->setCurrentRow(new_row_index);
    ui->chkFullWidth->setChecked(false);

    //Parse blue color words inside GUI element
    //set_color_words(o,ui->txtOriginal);
    set_color_words(t,ui->txtTranslation);
    //Set png fonts preview
    setPreview(script_entries_translation.at(new_row_index));

    //Set cursor at start position to prevent wrong coloring
    // set_text_cursor_start(ui->txtTranslation);
    set_text_cursor_start(ui->txtOriginal);
    //Update current row
    current_row_index=new_row_index;

    //Set no changes in translation, initially
    setTranslationChanged(false);
    //Disable update translation button
    ui->btnUpdateTrans->setEnabled(false);
    //Update translation lines number
    updateLinesNumber();

}

void MainWindow::setFontSize(Ui::MainWindow *myUi, int size){

    QFont font = myUi->txtOriginal->font();
    font.setPointSize(size);
    ui->txtOriginal->setFont(font);

    ui->txtTranslation->setFont(font);

    string fontStr="QTextEdit { font-size: ";

    fontStr.append(to_string(size));
    fontStr.append("px;}");

    ui->centralwidget->setStyleSheet(QString::fromStdString(fontStr));
}

void MainWindow::setCustomFontSize()
{
    bool ok;
    QString s_size = QInputDialog::getText(this,
                                           tr("Custom font size"),
                                           tr("Size:"),
                                           QLineEdit::Normal,
                                           tr(to_string(size).c_str()),               &ok);
    if (ok && !s_size.isEmpty()) {
        static int size=this->size;

        try {
            size=stoi(s_size.toStdString());
        } catch (const exception& ex ) {

        }

        setFontSize(ui,size);
    }
}

QString MainWindow::intToString(int num){
    QString s_num=to_string(num).c_str();
    return s_num;
}

void  MainWindow::saveChanges(){

    //Parse translation lines
    QStringList t=splitByLines(ui->txtTranslation->document());
    QStringList v;
    if(!by_lines){
        QString temp;
        for (unsigned int i=0;i<t.size();i++) {
            temp.append(t.at(i));
        }
        v.push_back(temp);
    }
    else{
        for (unsigned int i=0;i<(unsigned)t.length();i++) {
            if(!t.at(i).isEmpty()){
                v.push_back(t.at(i));
            }
        }
    }

    //Update new translation
    script_entries_translation.at(current_row_index)=v;
    //Update translation width
    script_entries_font_width.at(current_row_index)=this_row_width;
    //Update translation extra space
    script_entries_extra_space.at(current_row_index)=this_row_extra_space;

    //Disable update translation button
    ui->btnUpdateTrans->setEnabled(false);
    //Translation changes are not persistant in the file yet
    unsaved_changes=true;
    //Clear left dialogues list
    ui->lstEntries->clear();
    //Re-populate left dialogues list
    populateList();

    //Set png fonts preview
    setPreview(script_entries_translation.at(current_row_index));
    //Set cursor at start position to prevent wrong coloring
    set_text_cursor_start(ui->txtTranslation);
    set_text_cursor_start(ui->txtOriginal);
    setRecentProject();
}

QString MainWindow::getOpen(QString title,QString filter){
    QString myFilter=filter;
    QString myAllFilters=myFilter;
    myAllFilters.append(";; All files (*.*)");
    QString selFilter = filter;
    QString path = QFileDialog::getOpenFileName(
                this,
                title,
                home_dir.path(),
                myAllFilters,
                &selFilter
                );

    home_dir.setPath(path);
    return path;
}


QString MainWindow::getSave(QString title,QString filter,QString name){
    QString myFilter=filter;
    QString myAllFilters=myFilter;
    myAllFilters.append(";; All files (*.*)");
    QString selFilter = filter;
    QString path = QFileDialog::getSaveFileName(
                this,
                title,
                name,
                myAllFilters,
                &selFilter
                );
    return path;
}

unsigned int MainWindow::fromHex(const string &s) { return strtoul(s.c_str(), NULL, 16); }

QString MainWindow::toHex(unsigned int num)
{
    stringstream stream;
    stream <<  setfill('0')<<setw(pointer_size) <<hex<<num;
    string result( stream.str() );

    transform (result.begin(), result.end(), result.begin(), ::toupper);

    return QString::fromStdString(result);
}


QStringList MainWindow::splitByLines(const QTextDocument *doc){

    if(!doc){
        return QStringList();
    }
    QStringList ret;
    QTextBlock tb =doc->begin();

    while(tb.isValid()){
        QString blockText=tb.text();
        Q_ASSERT(tb.layout());
        if(!tb.layout()){
            continue;
        }
        for (int i=0;i!=tb.layout()->lineCount();i++) {
            QTextLine line =tb.layout()->lineAt(i);
            ret.push_back(blockText.mid(line.textStart(),line.textLength()));
        }
        tb=tb.next();
    }

    return ret;
}

void MainWindow::updateCharsNumber(){
    QString s(ui->txtTranslation->toPlainText());

    string text("Characters: ");
    text.append(to_string(s.size()));
    ui->lblCharCount->setText(QString::fromStdString(text));

}

void MainWindow::on_txtTranslation_textChanged()
{
    //Update translation lines number
    updateLinesNumber();
    //Update characters number
    updateCharsNumber();
    //Set no changes in translation
    setTranslationChanged(true);
    //Disable update translation button
    ui->btnUpdateTrans->setEnabled(true);
}

QMessageBox::StandardButton MainWindow::confirmCloseProject(QEvent *event){

    QMessageBox::StandardButton resBtn=QMessageBox::Ok;
    if(unsaved_changes||ui->btnUpdateTrans->isEnabled()){

        resBtn = QMessageBox::question( this, windowTitle(),
                                        tr("Save project and close?\n"),
                                        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        if (resBtn == QMessageBox::Yes) {
            if(translation_changed){
                saveChanges();
            }
            ui->actionSave_Project->trigger();

            if(event->type()==QCloseEvent::Close&&!script_save_path.isNull()){
                event->accept();
            }
            else{
                event->ignore();
            }
        } else if (resBtn == QMessageBox::Cancel){
            event->ignore();
        }
        else{
            if(event->type()==QCloseEvent::Close){
                event->accept();
            }
            else{
                event->ignore();
            }
        }
    }
    else if (translation_opened){

        resBtn = QMessageBox::question( this, windowTitle(),tr("Close project?\n"),
                                        QMessageBox::Yes | QMessageBox::No);

        if (resBtn == QMessageBox::Yes) {
            resBtn=QMessageBox::Ok;
            if(event->type()==QCloseEvent::Close){
                event->accept();
            }
            else{
                event->ignore();
            }
        }
        else{
            resBtn=QMessageBox::Cancel;
            event->ignore();
        }
    }
    else{
        event->accept();
    }

    return resBtn;
}

void MainWindow::closeEvent (QEvent *event)
{
    static QMessageBox::StandardButton result= confirmCloseProject(event);
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    static QMessageBox::StandardButton result= confirmCloseProject(event);
}

void MainWindow::findAndReplace(string & data, QString toSearch, QString replaceStr, bool all, bool char_mode)
{
    // Get the first occurrence
    size_t pos = data.find(toSearch.toStdString());
    // Repeat till end is reached
    while( pos != string::npos)
    {

        if (char_mode){
            int cplen = 1;
            if((data[pos] & 0xf8) == 0xf0) cplen = 4;
            else if((data[pos] & 0xf0) == 0xe0) cplen = 3;
            else if((data[pos] & 0xe0) == 0xc0) cplen = 2;
            if((pos + cplen) > data.length()) cplen = 1;

            data.replace(pos, cplen, replaceStr.toStdString());
        }
        else{
            data.replace(pos, toSearch.size(), replaceStr.toStdString());
        }


        // Replace this occurrence of Sub String
//        data.replace(pos, toSearch.size(), replaceStr.toStdString());
        if(!all){
            return;
        }
        // Get the next occurrence from the current position
        pos =data.find(toSearch.toStdString(), pos + replaceStr.size());
    }
}

//SFK CMD for Windows
#ifdef _WIN32
void MainWindow::executeCommand(string exec,string parameters){
    SHELLEXECUTEINFO ShExecInfo = {0};
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = exec.c_str();
    ShExecInfo.lpParameters = parameters.c_str();
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_HIDE;
    ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
    CloseHandle(ShExecInfo.hProcess);
}

int MainWindow::dumpBinaryToHex(string rawFile,string tempPath){

    string commandDumpBinToHex("/C sfk.exe hexdump -pure -nofile -nolf ");
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

    executeCommand("cmd.exe",commandDumpBinToHex);

    ifstream hexRead {tempPath };
    string stringHexRead {std::istreambuf_iterator<char>(hexRead), std::istreambuf_iterator<char>() };
    hexRead.close();

    //If size 0(false), negated (true)
    if(!stringHexRead.size()){
        return -1;
    }

    return 0;
}

int MainWindow::hexDumpToBinary(string hexdump_path,string temp_bin_path){

    string command_hexdump_to_binary("/C sfk.exe filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    executeCommand("cmd.exe",command_hexdump_to_binary);

    ifstream hexRead {hexdump_path };
    string stringHexRead {std::istreambuf_iterator<char>(hexRead), std::istreambuf_iterator<char>() };
    hexRead.close();

    //If size 0(false), negated (true)
    if(!stringHexRead.size()){
        return -1;
    }

    return 0;
}

//SFK CMD for Non OSX (or Linux*)
#else
void MainWindow::executeCommand(string exec,string parameters){
    string command=exec+" "+parameters+" -quiet";
    std::system(command.c_str());
}

int MainWindow::dumpBinaryToHex(string rawFile,string tempPath){
    string commandDumpBinToHex(" hexdump -pure -nofile -nolf ");
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

    executeCommand("sfk",commandDumpBinToHex);

    ifstream hexRead {tempPath };
    string stringHexRead {std::istreambuf_iterator<char>(hexRead), std::istreambuf_iterator<char>() };
    hexRead.close();

    //If size 0(false), negated (true)
    if(!stringHexRead.size()){
        return -1;
    }

    return 0;
}

int MainWindow::hexDumpToBinary(string hexdump_path,string temp_bin_path){
    string command_hexdump_to_binary(" filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    executeCommand("sfk",command_hexdump_to_binary);

    ifstream hexRead {hexdump_path };
    string stringHexRead {std::istreambuf_iterator<char>(hexRead), std::istreambuf_iterator<char>() };
    hexRead.close();

    //If size 0(false), negated (true)
    if(!stringHexRead.size()){
        return -1;
    }

    return 0;
}
#endif

void MainWindow::convertEndianness(unsigned int byteSize){

    QString filterHexDump="HexDump (*.txt *.dat *.ext *.incs *.exe)";
    QString titleHexSwapEndianness("Choose a HexDump file");

    QString fileHexToSwappedEndiannessPath=getOpen(titleHexSwapEndianness,filterHexDump);

    //If size 0(false), negated (true)
    if(!fileHexToSwappedEndiannessPath.size()){
        return;
    }

    std::ifstream hexRead {fileHexToSwappedEndiannessPath.toStdString() };
    std::string stringHexRead {std::istreambuf_iterator<char>(hexRead), std::istreambuf_iterator<char>() };
    hexRead.close();

    QString swappedEndiannessString=getLEString(stringHexRead,true,true,byteSize);

    //If size 0(false), negated (true)
    if(!swappedEndiannessString.size()){
        return;
    }

    //Get info specified path
    QFileInfo info(fileHexToSwappedEndiannessPath);
    //Prepare default export path
    /*
    QDir path_export_conversion(info.absolutePath()+QDir::separator()+info.baseName()+
                                converted_prefix+QString::fromStdString(to_string(byteSize))+byte_prefix+
                                info.suffix());
*/
    //Prepare temporal path
    QDir pathTempConverted(temp_folder_path+QDir::separator()+info.baseName()+
                           converted_prefix+QString::fromStdString(to_string(byteSize))+byte_prefix+
                           info.suffix());

    QString selfilterHexDump("HexDump (*.dat *.incs *.txt)");
    QString titleSave("Export Endianness Reversed HexDump");
    //Allow user to specify the export path
    QString fileNameExportHex=getSave(titleSave,selfilterHexDump,pathTempConverted.path());

    //Save in export path
    std::ofstream convertedEndianness;
    convertedEndianness.open(fileNameExportHex.toStdString());
    writeFile(convertedEndianness,swappedEndiannessString);
    convertedEndianness.close();

    QMessageBox msgBox;
    QString msg("Conversion complete!");
    msgBox.setText(msg);
    msgBox.exec();
    cout<<msg.toStdString()<<endl;
}

void replace_all(QStringList &list,vector<bool> &flags,unsigned int start_index, QString find, QString replace){
    for (unsigned int i=start_index;i<(unsigned int)list.size();i++) {
        bool is_modified=flags.at(i);
        if(list.at(i)==find&&!is_modified){
            list.replace(i,replace);
            flags.at(i)=true;
        }
    }
}

void MainWindow::insertTranslation(QString file_to_patch_path,QString original_pointers_section_path, QString translation_pointers_path,
                                   QString original_text_section_path,QString translation_text_path,QString out_path,QString original_pointers_path){

    //Set all five paths to the files specified by the user
    QDir file_to_patch_path_dir(file_to_patch_path),
            original_pointers_section_path_dir(original_pointers_section_path),
            translation_pointers_path_dir(translation_pointers_path),
            original_text_section_path_dir(original_text_section_path),
            translation_text_path_dir(translation_text_path),
            original_pointers_path_dir(original_pointers_path);

    //Read original pointers section -- Size might be different than the translation pointers section
    ifstream pointers_original_section_stream {original_pointers_section_path_dir.path().toStdString() };
    string pointers_original_section_h {istreambuf_iterator<char>(pointers_original_section_stream), std::istreambuf_iterator<char>() };
    pointers_original_section_stream.close();
    //Read original text section
    ifstream text_original_section_stream {original_text_section_path_dir.path().toStdString() };
    string text_original_section_h {std::istreambuf_iterator<char>(text_original_section_stream), std::istreambuf_iterator<char>() };
    text_original_section_stream.close();

    //Read pointers original pointers
    ifstream pointers_original_stream {original_pointers_path_dir.path().toStdString() };
    string pointers_original_h {std::istreambuf_iterator<char>(pointers_original_stream), std::istreambuf_iterator<char>() };
    pointers_original_stream.close();

    //Read  translation pointers
    ifstream pointers_translation_stream {translation_pointers_path_dir.path().toStdString() };
    string pointers_translation_h {std::istreambuf_iterator<char>(pointers_translation_stream), std::istreambuf_iterator<char>() };
    pointers_translation_stream.close();
    //Read translation text
    ifstream text_translation_stream {translation_text_path_dir.path().toStdString() };
    string text_translation_h {std::istreambuf_iterator<char>(text_translation_stream), std::istreambuf_iterator<char>() };
    text_translation_stream.close();

    //Separate the file name path nodes for later
    QFileInfo info(file_to_patch_path_dir.path());

    //Construct the export name using the currentProject name,a custom text and the filname extension
    //   QString name_patched_export(info.baseName()+patched_prefix+info.suffix());

    //Prepare a filename path pointing to the temporal files folder
    QDir temp_binary_dir(temp_folder_path+QDir::separator()+info.baseName()+patched_prefix+info.suffix());
    //    pathTempPatched.append(namePatchedExport);

    //If a filename under the same name exists in the temporal folder, delete it
    if (QFile::exists(temp_binary_dir.absolutePath()))
    {
        QFile::remove(temp_binary_dir.absolutePath());
    }
    //Copy the original clean file to patch to the temporal files folder
    QFile::copy(file_to_patch_path_dir.absolutePath(), temp_binary_dir.absolutePath());

    //    QString name_pointers_bin_hex(info.baseName()+hex_prefix+info.suffix());
    QDir temp_bin_to_hex_dir(temp_folder_path+QDir::separator()+info.baseName()+hex_prefix+info.suffix());

    if(dumpBinaryToHex(temp_binary_dir.path().toStdString(),temp_bin_to_hex_dir.path().toStdString())){
        if(showMessages){
            if(!(temp_bin_to_hex_dir.path().length())){
                QMessageBox msgBox;
                QString msg("Injection failed! Error Dumping the binary");
                msgBox.setText(msg);
                msgBox.exec();                
                cout <<msg.toStdString()<<endl;
                return;
            }
        }
    }

    //Read bin HEXDUMP
    ifstream binary_hex_stream {temp_bin_to_hex_dir.path().toStdString() };
    string binary_hex {std::istreambuf_iterator<char>(binary_hex_stream), std::istreambuf_iterator<char>() };
    binary_hex_stream.close();
    // QDir temp_hex_patched_dir(temp_folder_dir.path()+QDir::separator()+info.baseName()+patched_prefix+info.suffix());
    //cout <<temp_hex_patched_dir.path().toStdString()<<endl;

    // Step #1 Replace the table section

    // * Replace the table section pointer by pointer
    if(multi_mode||first_mode||unordered_mode){
        // Convert the original pointers section to a string list
        QStringList pointers_original_section;
        vector <bool> pointers_flags_original_section;
        for(unsigned int i=0;i<pointers_original_section_h.length();i+=pointer_size){
            string this_pointer_original_section=pointers_original_section_h.substr(i,pointer_size);
            pointers_original_section.push_back(QString::fromStdString(this_pointer_original_section));
            pointers_flags_original_section.push_back(false);
        }
        //Create a list with all the original pointers
        QStringList pointers_original;
        //Create a list with the translation pointers
        QStringList pointers_translation;
        for(unsigned int i=0;i<pointers_original_h.length();i+=pointer_size){
            string pointer_original=pointers_original_h.substr(i,pointer_size);
            string pointer_translation=pointers_translation_h.substr(i,pointer_size);
            pointers_original.push_back(QString::fromStdString(pointer_original));
            pointers_translation.push_back(QString::fromStdString(pointer_translation));
        }
        //Replace the original pointers by the translation pointers in the
        //original pointers section
        //unsigned int next_ptr_original_section=0,
        unsigned int next_ptr_index=0;

        for (unsigned int j=0;j<(unsigned int)pointers_original_section.size();j++) {
            static bool found_ocurrence=false;
            static unsigned int i;
            if(!unordered_mode){
                i=next_ptr_index;
            }
            else{
                i=0;
            }
            for(i=next_ptr_index;i<(unsigned int)pointers_original.size();i++){
                QString pointer_original_section=pointers_original_section.at(j);
                QString pointer_original=pointers_original.at(i);
                bool is_modified=pointers_flags_original_section.at(j);
                if(pointer_original==pointer_original_section&&!is_modified){
                    QString pointer_translation=pointers_translation.at(i);
                    pointers_original_section.replace(j,pointer_translation);
                    pointers_flags_original_section.at(j)=true;

                    //If the first ocurrence of the pointers has been found
                    if(!found_ocurrence){
                        //On the next iteration don't evaluate original ptrs which were already compared
                        //Prevents to overwritte ptrs, works for pointers that are in incremental order!

                        //Position set to next ptr
                        next_ptr_index=i;
                        //Next ptr to compare set to the next in the list of original pointers
                        //next_ptr_original_section=j;
                        found_ocurrence=true;
                    }

                    //If multi mode, replace them all starting from the current position
                    if(multi_mode){
                        replace_all(pointers_original_section,pointers_flags_original_section,j,pointer_original,pointer_translation);
                        break;
                    }
                    //If unordered, replace them even if they are located in different parts of the section, works for ptrs which are in disorder
                    else if(unordered_mode){
                        replace_all(pointers_original_section,pointers_flags_original_section,0,pointer_original,pointer_translation);
                        break;
                    }
                    //Otherwise, replace first ocurrence only
                    //Replace first ocurrence only, if first mode
                    else{
                        next_ptr_index++;
                        break;
                    }
                    //Doesnt reach this part
                    //Otherwise, continue looking for more ocurrences
                }
            }

        }

        QString translation_all_pointers_section_h;
        for (unsigned int j=0;j<(unsigned int)pointers_original_section.size();j++) {
            translation_all_pointers_section_h.append(pointers_original_section.at(j));
        }


        //cout <<translation_all_pointers_section_h.toStdString()<<endl;
        //Replace all original pointers by the translation pointers
        findAndReplace(binary_hex,QString::fromStdString(pointers_original_section_h),translation_all_pointers_section_h,true,false);
    }
    // Replace all the pointers table/section in a single command
    // Only use if original section and translation section are the same size
    else {
        if(pointers_original_section_h.size()!=pointers_translation_h.size()){
            if(showMessages){
                QMessageBox msgBox;
                QString msg("Pointers lengths don´t match!\n Please use pointer width mode");
                msgBox.setText(msg);
                cout <<msg.toStdString()<<endl;
                msgBox.exec();
            }            
            return;
        }

        findAndReplace(binary_hex,QString::fromStdString(pointers_original_section_h),QString::fromStdString(pointers_translation_h),false,false);

    }
    // #Step 2
    //Replace text section


    findAndReplace(binary_hex,QString::fromStdString(text_original_section_h),QString::fromStdString(text_translation_h),false,false);

    //Save patched hex in temporal file
    ofstream all_patched_stream;
    all_patched_stream.open(temp_bin_to_hex_dir.absolutePath().toStdString());
    writeFile(all_patched_stream,QString::fromStdString(binary_hex));
    all_patched_stream.close();


    if(!(temp_bin_to_hex_dir.path().length())){
       if(showMessages){
            QMessageBox msgBox;
            QString msg("Injection failed!");
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
            msgBox.exec();
        }
       return;
    }

    //Hexdump to binary
    QDir temp_hex_to_bin_dir(temp_folder_path+QDir::separator()+info.baseName()+patched_binary_prefix+info.suffix());
    hexDumpToBinary(temp_bin_to_hex_dir.path().toStdString(),temp_hex_to_bin_dir.path().toStdString());

    //Prepare the open file dialog parameters
    QString filter("Game file (*.bin *.ext *.dat *.incs *.loctext)");
    QString titleSave("Patched file");

    //Ask for a path where to save the patched file
    QString patched_export_path=out_path;
    if(out_path.isEmpty()){
        patched_export_path=getSave(titleSave,filter,(info.baseName()+patched_binary_prefix+info.suffix()));
    }

    //Stop if no path specified
    if(patched_export_path.isNull()){
        if(!(temp_bin_to_hex_dir.path().length())){
           if(showMessages){
                QMessageBox msgBox;
                QString msg("Injection failed!");
                msgBox.setText(msg);
                cout <<msg.toStdString()<<endl;
                msgBox.exec();
            }
            return;
        }
    }
    //If an old file with the same name exists, remove it
    if (QFile::exists(patched_export_path))
    {
        QFile::remove(patched_export_path);
    }

    //Copy the patched file from the temporal folder to the user choosen path
    QFile::copy(temp_hex_to_bin_dir.path(), patched_export_path);

    if(showMessages){
        QMessageBox msgBox;
        QString msg("Translation inserted!");
        msgBox.setText(msg);
        cout <<msg.toStdString()<<endl;
        msgBox.exec();
    }
}

QStringList MainWindow::script_entries_to_export(vector < QStringList> script_entries){
    QStringList e;
    //Create single lines for every dialogue, separated by a line break
    for (unsigned int i=0;i<script_entries.size();i++) {
        QString entry;
        for (unsigned int j=0;j<(unsigned int)script_entries.at(i).size();j++) {
            entry.append(script_entries.at(i).at(j));
            //Append line break at the end of every line, except last
            if(!(j==(unsigned int)script_entries.at(i).size()-1)){
                entry.append(ch_line_break_escape);
            }
        }

        //if(!dialogue.isEmpty()){
            e.push_back(entry);
        //}

    }
    return e;
}

unsigned int checkTranslationComplete(){
    if(untranslated){
        return 1;
    }
    else{
        return 0;
    }
}

unsigned int MainWindow::msg_check_export_entries(){
    QMessageBox::StandardButton res=QMessageBox::Ok;

    res = QMessageBox::question( this, windowTitle(),tr("There are untranslated dialogues\n¿Export anyway?\n"),
                                 QMessageBox::Yes | QMessageBox::No);
    if (res == QMessageBox::No) {
        return 1;
    }
    else{
        return 0;
    }
}

void MainWindow::exportTranslationTextToHex(){

    //Check if all dialogues are translated
    if(checkTranslationComplete()){

        if(msg_check_export_entries()){
            return;
        }

    }

    QString table_filter,table_title;
            //QString table_dir;
    if(!table_loaded){

        if(showMessages){
            QMessageBox msgBox;
            QString msg("No table loaded!");
            msgBox.setText(msg);
            msgBox.exec();
            cout <<msg.toStdString()<<endl;
        }

        if(table_path.isEmpty()){
            table_filter="Table (*.tbl *.txt)";
            table_title="Open table";
            table_path=getOpen(table_title,table_filter);
        }

        if(table_path.isEmpty()){
            return;
        }
    }

    last_used_path = QFileInfo(table_path).path();
    //Read the table characters
    ifstream tableStream {table_path.toStdString() };
    string str;
    vector<string> tableList;
    while (getline(tableStream, str))
    {
        tableList.push_back(str);
    }

    //Prepare the table codes and characters list
    if(prepareTables(tableList)){
        return;
    }

    QDir path_export_text_dir(project_root_absolute_path+QDir::separator()+project_base_name+script_translation_export_suffix);
    QString hexdump_filter,save_title;
    if(text_export_path.isEmpty()){
        hexdump_filter="HexDump (*.txt)";
        save_title="Export Translation";
        text_export_path=getSave(save_title,hexdump_filter,path_export_text_dir.path());
    }

    //If size 0(false), negated (true)
    if(!text_export_path.size()){
        return;
    }

    string translation_hex("");
    QStringList script_entries_export_format=script_entries_to_export(script_entries_translation);

    // The \n line breaks are replaced automatically
    //Convert the translation to hex dump code, including end entry code 0000
    for (unsigned int i=0;i<(unsigned int)script_entries_export_format.size();i++) {
        string this_entry=script_entries_export_format.at(i).toStdString();

        //Alien DS Exclamation marks fix
        if(special_settings){
            findAndReplace(this_entry,"¿","%?",true,true);
            findAndReplace(this_entry,"¡","%!",true,true);
        }

        for (size_t  j=0;j<this_entry.length();) {
            int cplen = 1;
            if((this_entry[j] & 0xf8) == 0xf0) cplen = 4;
            else if((this_entry[j] & 0xf0) == 0xe0) cplen = 3;
            else if((this_entry[j] & 0xe0) == 0xc0) cplen = 2;
            if((j + cplen) > this_entry.length()) cplen = 1;
            string this_char=this_entry.substr(j,cplen);
            j += cplen;

            //Check if spanish characters

            bool spanishCharacter=false;

            for (unsigned int k=0;k<(unsigned int)lat_chars.size();k++) {
                if(lat_chars.at(k).toStdString()==this_char){
                    spanishCharacter=true;
                    break;
                }
            }

            //Start text conversion
            for (unsigned k=0;k<table_characters.size();k++) {
                if(this_char==table_characters.at(k)){
                    if(script_entries_font_width.at(i)&&!spanishCharacter){
                        translation_hex.append(table_codes.at(k+entry_width));
                    }
                    else{
                        translation_hex.append(table_codes.at(k));
                    }
                    break;
                }
            }

        }

        //Replace [A] three characters (6 bytes) by the correct hex character (2 bytes)
        findAndReplace(translation_hex,s_A_half,s_A,true,false);

        //Append dialogue delimiter at the end of every dialogue
        translation_hex.append(entry_delimiter.toStdString());

        //Settings for Alien Isolation
        //Replace a special character in the dialogue
        if(special_settings){
            findAndReplace(translation_hex,sc_alien,sc_alien_hex,true,false);
        }
    }

    //Settings for Alien Isolation

    //Replace ¿  and ¡ custom hex codes for the proper ones
        string new_translation_replaced_hex_codes;
        bool found_first=false;
        for(unsigned int j=0;j<translation_hex.length();j+=sizeone){
            string this_char=translation_hex.substr(j,sizeone);

            if(!found_first){
                if(this_char==codeFirst.toStdString()){
                    found_first=true;
                }
                else{
                    new_translation_replaced_hex_codes.append(this_char);
                }
            }
            else {
                if(this_char==codeScnd.toStdString()){
                    new_translation_replaced_hex_codes.append(full_latin1.toStdString());
                }
                else if(this_char==codeThrd.toStdString()){
                    new_translation_replaced_hex_codes.append(full_latin2.toStdString());
                }
                else{
                    new_translation_replaced_hex_codes.append(codeFirst.toStdString());
                    new_translation_replaced_hex_codes.append(this_char);
                }
                found_first=false;
            }
        }

        translation_hex=new_translation_replaced_hex_codes;

        /*
    }

    */

    //Export conversion
    ofstream text_hex_stream;
    text_hex_stream.open(text_export_path.toStdString());
    writeFile(text_hex_stream,QString::fromStdString(translation_hex));
    text_hex_stream.close();
    text_export_path.clear();
    if(showMessages){
        QMessageBox msgBox;
        QString msg("Text HexDump exported!");
        msgBox.setText(msg);
        msgBox.exec();
        cout <<msg.toStdString()<<endl;
    }
}


void MainWindow::exportTranslationPointersToHex(vector < QStringList> script_entries){

    if(checkTranslationComplete()){

        if(msg_check_export_entries()){
            return;
        }

    }
    //string first_pointer,myPathNotUsed;
    //first_pointer=pointers_offset.toStdString();
    // bool  export_flag;
    if(pointers_export_path.isEmpty()){
        //Start the window
        myexportpointers = new exportpointers;
        //Pass global pointer size to the export pointers object
        myexportpointers->pointer_size=pointer_size;
        //Pass global byte zero string to the export pointers object
        myexportpointers->bytezero=bytezero;
        //Fill default first pointer offset as '00 00 00 00'
        QString p("");
        for (unsigned int i=0;i<pointer_size/2;i++) {
            p.append(bytezero);
            for (unsigned int j=i+1;j<pointer_size/2;j++) {
                p.append(" ");
                break;
            }
        }
        myexportpointers->setFirstPointer(p);
        myexportpointers->exec();

        //Read window preferences
        pointers_offset=myexportpointers->firstPointer;
 //       myPathNotUsed=myexportpointers->pathExclude.toStdString();
        pointers_absolute=myexportpointers->absolutePointersFlag;
        bool export_flag=myexportpointers->exportFlag;

        //If event rejected, not continue
        if(!export_flag){
            return;
        }
   //     bool  myExcludeFlag=myexportpointers->excludeFlag;
        home_dir.setPath(myexportpointers->home_export_path);

        QDir path_export_pointers(project_root_absolute_path+QDir::separator()+project_base_name+script_pointers_export_suffix);
        QString hexdump_filter("HexDump (*.txt)");
        QString save_title("Export Pointers");
        pointers_export_path=getSave(save_title,hexdump_filter,path_export_pointers.path());
        home_dir.setPath(pointers_export_path);
    }

    if(pointers_export_path.isNull()){
        return;
    }

    QStringList script_entries_export=script_entries_to_export(script_entries);

    //Generate new pointers by word length
    unsigned int first_ptr_base10= fromHex(pointers_offset.toStdString());
    string new_pointers;
    new_pointers.append(pointers_offset.toStdString());

    unsigned int w_length=0;
    for (unsigned int i=0;i<(unsigned int)script_entries_export.size()-1;i++) {

        //Not append last pointer when it is empty
        //Necessary to rebuild Monster Tale DS
//        if(!script_entries_export.at(i+1).toStdString().length()&&((i+1)==(unsigned int)script_entries_export.size()-1)){
//            break;
//        }

        string this_entry=script_entries_export.at(i).toStdString();
        QString next_ptr_base16;

        //Replace Question marks in Aliens DS before exporting
        //To match game character codes
        //Alien DS Exclamation marks fix
        if(special_settings){
            findAndReplace(this_entry,"¿","%?",true,true);
            findAndReplace(this_entry,"¡","%!",true,true);
        }

        unsigned int total_chars = 0;
        for(size_t j = 0; j < this_entry.length();)
        {
            int cplen = 1;
            if((this_entry[j] & 0xf8) == 0xf0) cplen = 4;
            else if((this_entry[j] & 0xf0) == 0xe0) cplen = 3;
            else if((this_entry[j] & 0xe0) == 0xc0) cplen = 2;
            if((j + cplen) > this_entry.length()) cplen = 1;

            total_chars++;
            j += cplen;

        }

        //Check [A] occurrences
        unsigned int occurrences = 0;
        string::size_type pos = 0;
        while ((pos = this_entry.find(ccode1.toStdString(), pos )) != string::npos) {
            ++ occurrences;
            pos += ccode1.length();
        }
        total_chars-=(occurrences*2);

        //Check %a% occurrences
        if(special_settings){
            unsigned int occurrences1 = 0;
            pos=0;
            while ((pos = this_entry.find(ccode2.toStdString(), pos )) != string::npos) {
                ++ occurrences1;
                pos += ccode2.length();
            }
            total_chars-=(occurrences1*2);

            //Evaluate latin characters pre hex codes
            for (unsigned int j=0;j<(unsigned int)lat_chars.size();j++) {
                string this_target = lat_chars.at(j).toStdString();
                unsigned int occurrencesES = 0;
                unsigned int extra_bytes=0;
                pos=0;
//                cout<<this_entry<<endl;
//                cout<<this_target<<endl;
                while ((pos = this_entry.find(this_target, pos )) != string::npos) {
                    extra_bytes+=(lat_chars_h_codes.at(j).size()-2)/2;
                    ++ occurrencesES;
                    pos += this_target.length();
//                    cout<<"Occurrences: "<<occurrencesES<<endl;
                }
//                total_chars+=(occurrencesES);
                total_chars+=(extra_bytes);

//                cout<<total_chars<<endl;
            }
        }

        //Start pointers generation
        w_length=w_length+((total_chars+1)*(char_size/2));

        //Append pointers
        if(pointers_absolute){
            //unsigned int next_ptr_base10=;
            next_ptr_base16=toHex(first_ptr_base10+w_length);
        }
        else{
            next_ptr_base16=toHex(w_length);
        }

        bool thisUsed=true;

        if(thisUsed){
            new_pointers.append(next_ptr_base16.toStdString());
        }

    }
    //End of pointers generation

    //Convert new pointers to little endian format
    QString little_endian_pointers=getLEString(new_pointers,true,true,pointer_size/2);
    //Export pointers
    ofstream exportPointersHexStream;
    exportPointersHexStream.open(pointers_export_path.toStdString());
    writeFile(exportPointersHexStream,little_endian_pointers);
    exportPointersHexStream.close();
    pointers_export_path.clear();

    if(showMessages){
        QMessageBox msgBox;
        QString msg("Pointers HexDump exported!");
        msgBox.setText(msg);
        msgBox.exec();
        cout <<msg.toStdString()<<endl;
    }

}

QStringList MainWindow::stringVector_to_QStringList(vector <string> v){
    QStringList ql;
    for (unsigned int i=0;i<(unsigned)v.size();i++) {
        ql.push_back(QString::fromStdString(v.at(i)));
    }
    return ql;
}


void MainWindow::setTranslationChanged(bool b){
    translation_changed=b;
    ui->btnUpdateTrans->setEnabled(b);
}

void MainWindow::setPreview(QStringList strs){

    try {
        text_window_path=QString::fromStdString(win_imgs[to_string(ui->cbWin->currentIndex()+1)]["file"].get<string>());
        font_space_offset=win_imgs[to_string(ui->cbWin->currentIndex()+1)]["start_offset"].get<int>();
    } catch (exception ex) {
        if(showMessages){
            QString msg("Error reading win.json file...");
            QMessageBox msgBox;
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
            return;
        }
    }
    auto this_win_pos=win_imgs[to_string(ui->cbWin->currentIndex()+1)]["pos_y"];

    //Set background picture
    QImage pic(res_path+QDir::separator()+win_dir_path+QDir::separator()+text_window_path);

    //QString current_color;
    QPainter painter(&pic);
    int font_pos_offset;
    for(unsigned int i=0;i<(unsigned int)strs.size()&&i<max_lines;i++){
        //current_color=n;
        bool color=false;
        unsigned int sc_counter=0;
        font_pos_offset=font_space_offset;

        for (unsigned int j=0;j<(unsigned int)strs.at(i).length();j++) {
            //Toggle color on open delimiter - close delimiter found
            for (unsigned int k=0;k<sc_blue_words_delimiters.size();k++) {
                if(strs.at(i).toStdString().substr(j,2)==sc_blue_words_delimiters.at(k).toStdString()){
                    sc_counter+=2;
                    j+=2;
                    color=!color;
                    break;
                }
            }
            //If no characters after delimiter, stop
            if(j>=(unsigned int)strs.at(i).size()){
                break;
            }
            //Set which color to use
            // current_color=png_colors.at(color);

            try {
                QString f=QString::fromStdString(win_imgs[to_string(ui->cbWin->currentIndex()+1)][string("font_").append(to_string(color))].get<string>());

                QDir dir(res_path+QDir::separator()+fonts_path+QDir::separator()+f+QDir::separator()+QString::fromStdString(fonts[strs.at(i).toStdString().substr(j,1)].get<string>()));
                QImage font_char(dir.path());

                unsigned int pad_left=0,pad_right=0;

                int char_matrix[font_char.height()][font_char.width()];
                for (unsigned int k=0;k<(unsigned int)font_char.width();k++) {
                    for (unsigned int l=0;l<(unsigned int)font_char.height();l++) {
                        char_matrix[l][k]=qGray(font_char.pixel(k,l));
                    }
                }
                for (unsigned int k=0;k<(unsigned int)font_char.width();k++) {
                    for (unsigned int l=0;l<(unsigned int)font_char.height();l++) {
                        if(char_matrix[l][k]){
                            pad_left=k;
                            goto found_lp;
                        }
                    }
                }
                found_lp:

                for (int k=font_char.width()-1;k>=0;k--) {
                    for (int l=font_char.height()-1;l>=0;l--) {
                          if(char_matrix[l][k]){
                            pad_right=(k+1);
                            goto found_rp;
                        }
                    }
                }

                found_rp:
                if(font_pos_offset-pad_left>=0){
                    font_pos_offset-=pad_left;
                }

                //Draw the letters
                painter.drawImage(QPoint(font_pos_offset,this_win_pos.at(i)),font_char);

                if(!pad_right){
                    font_pos_offset+=space_char_w;
                }
                else{
                    font_pos_offset+=pad_right+ch_s;
                }

            } catch (exception e) {

            }

        }

    }
    painter.end();
    unsigned int width=ui->imageTalk->width();
    unsigned int height=ui->imageTalk->height();

    //Set the image
    ui->imageTalk->setMargin(5);
    ui->imageTalk->setAlignment(Qt::AlignCenter);
    ui->imageTalk->clear();
    ui->imageTalk->setPixmap(QPixmap::fromImage(pic).scaled(width*0.9,height*0.9,Qt::KeepAspectRatio));

}

void MainWindow::init_editor_module(){
    QDir dir_module(modules_path+QDir::separator()+module_file_path);

    INIReader reader(dir_module.absolutePath().toStdString());

    if (reader.ParseError()) {
        QMessageBox msgBox;
        QString msg("Can't load '"+module_file_path+"'");
        msgBox.setText(msg);
        cout << msg.toStdString()<<endl;
        return;
    }

    if(showMessages){
        cout <<"Module loaded!"<<endl;
    }
    QSettings settings(dir_module.absolutePath(),QSettings::IniFormat);
    settings.sync();
    settings.beginGroup("module");
    sc_alien=settings.value("sc_alien").toString();
    entry_delimiter=settings.value("dialogue_delimiter").toString();
    entry_delimiter_custom=settings.value("dialogue_delimiter_custom").toString();
    s_A_half=settings.value("s_A_half").toString();
    s_A_full=settings.value("s_A_full").toString();
    s_A=settings.value("s_A").toString();
    prefix_latin_char_hex=settings.value("prefix_latin_char_hex").toString();
    sc_alien_hex=settings.value("sc_alien_hex").toString();
    full_latin1=settings.value("full_latin1").toString();
    full_latin2=settings.value("full_latin2").toString();
    codeFirst=settings.value("codeFirst").toString();
    codeScnd=settings.value("codeScnd").toString();
    codeThrd=settings.value("codeThrd").toString();
    /*
    codeFullReplace=settings.value("codeFullReplace").toString();
    codeFullReplace2=settings.value("codeFullReplace2").toString();
    */
    ccode1=settings.value("ccode1").toString();
    ccode2=settings.value("ccode2").toString();
    char_size=settings.value("character_size").toInt();
    special_settings=settings.value("special_settings").toInt();
    settings.endGroup();

}

unsigned int MainWindow::init(){

    QDir dir_config(QCoreApplication::applicationDirPath()+QDir::separator()+config_ini_file);

    INIReader reader(dir_config.absolutePath().toStdString());

    if (reader.ParseError()) {
        QMessageBox msgBox;
        QString msg("Can't load '"+config_ini_file+"'");
        msgBox.setText(msg);
        cout << msg.toStdString()<<endl;
        return 1;
    }

    QSettings settings(dir_config.absolutePath(),QSettings::IniFormat);

    settings.sync();
    settings.beginGroup("path");
    temp_folder_path=settings.value("temp_folder_path").toString();
    temp_folder_path=QCoreApplication::applicationDirPath()+QDir::separator()+temp_folder_path;
    res_path=settings.value("res_path").toString();
    fonts_path=settings.value("fonts_path").toString();
    fonts_table_path=settings.value("fonts_table_path").toString();    
    win_dir_path=settings.value("win_dir_path").toString();
    win_json_path=settings.value("win_path").toString();
    text_window_path=settings.value("text_window_path").toString();
    modules_path=settings.value("modules_path").toString();
    modules_path=QCoreApplication::applicationDirPath()+QDir::separator()+modules_path;
    settings.endGroup();

    settings.beginGroup("suffix");
    temp_copy_suffix=settings.value("temp_copy_suffix").toString();
    script_translation_export_suffix=settings.value("script_translation_export_suffix").toString();
    script_pointers_export_suffix=settings.value("script_pointers_export_suffix").toString();
    binary_prefix=settings.value("binary_prefix").toString();
    hex_prefix=settings.value("hex_prefix").toString();
    project_suffix=settings.value("project_suffix").toString();
    converted_prefix=settings.value("converted_prefix").toString();
    byte_prefix=settings.value("byte_prefix").toString();
    patched_prefix=settings.value("patched_prefix").toString();
    patched_binary_prefix=settings.value("patched_binary_prefix").toString();

    settings.endGroup();

    settings.beginGroup("default");
    font_size_default=settings.value("font_size_default").toInt();
    list_font_size_default=settings.value("list_font_size_default").toInt();
    extra_space_width=settings.value("extra_space_width").toInt();
    normal_space_width=settings.value("normal_space_width").toInt();
    entry_width=settings.value("dialogue_width").toInt();
    max_lines=settings.value("max_lines").toInt();
    font_space_offset=settings.value("font_space_offset").toInt();
    font_width=settings.value("font_width").toInt();
    pointer_size=settings.value("pointer_size").toInt();
    sizetwo=settings.value("sizetwo").toInt();
    sizeone=settings.value("sizeone").toInt();
    //sc_size=settings.value("special_character_size").toInt();
    space_char_w=settings.value("space_char_width").toInt();
    ch_s=settings.value("ch_separation").toInt();
    pointers_offset=settings.value("pointers_default_offset").toString();

    //string font_positionsy= settings.value("font_positionsy").toString().toStdString();
    settings.endGroup();

    init_recent_projects_paths();
    QFile inputFile(QCoreApplication::applicationDirPath()+QDir::separator()+"latin.txt");
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
//       in.setCodec("UTF-8");
       while (!in.atEnd())
       {
          QString line = in.readLine();
//          cout<<line.toStdString();
          QString code=line.split("=")[0];
          QString character=line.split("=")[1];
//          cout<<code.toStdString()<<endl;
//          cout<<character.toStdString()<<endl;

          lat_chars_h_codes.push_back(code);
          lat_chars.push_back(character);
       }
       inputFile.close();
    }

//    cout <<"Size chars: "<<lat_chars.size()<<endl;
//    cout <<"Size codes: "<<lat_chars_h_codes.size()<<endl;

    //Populate spanish characters hex codes (1 byte)

    lat_chars_h_codes.push_back("BF"); //¿
    lat_chars_h_codes.push_back("A1"); //¡

    lat_chars.push_back("¿"); //¿
    lat_chars.push_back("¡"); //¡


    sc_blue_words_delimiters.push_back("%9");
    sc_blue_words_delimiters.push_back("%0");

    QDir dir_win(QCoreApplication::applicationDirPath()+QDir::separator()+res_path+QDir::separator()+win_json_path);

    ifstream stream_win(dir_win.absolutePath().toStdString());

    try {
        stream_win>>win_imgs;
        stream_win.close();
    } catch (exception ex) {
        if(showMessages){
            QMessageBox msgBox;
            QString msg("Failed to load the win images..");
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
        }
        stream_win.close();
    }

    for (unsigned int i=0;i<win_imgs.size();i++) {
       ui->cbWin->addItem(QString::fromStdString(win_imgs[to_string(i+1)]["name"].get<string>()));
    }


    QDir dir_fonts(QCoreApplication::applicationDirPath()+QDir::separator()+res_path+QDir::separator()+fonts_table_path);

    ifstream stream_fonts(dir_fonts.absolutePath().toStdString());

    try {
        stream_fonts>>fonts;
        stream_fonts.close();
    } catch (exception ex) {
        if(showMessages){
            QMessageBox msgBox;
            QString msg("Failed to load the fonts...");
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
        }
        stream_fonts.close();
    }
    create_temp_dir();

    setFontSize(ui,font_size_default);
    ui->lstEntries->setFont(ui->txtOriginal->font());
    ui->actionSave_Project->setEnabled(false);
    ui->actionExport_Text_to_HexDump->setEnabled(false);
    ui->actionExport_Pointers_to_HexDump->setEnabled(false);
    ui->actionExport_Original_Pointers_to_HexDump->setEnabled(false);
    setWindowTitle(project_base_name+ w_title);
    setWindowIcon(QIcon(":/icon.ico"));

    //Fix HOME folder
    #ifdef _WIN32
        home_dir.setPath(QString::fromStdString(getenv("USERPROFILE"))+QString::fromStdString("/Documents"));
    #else
        home_dir.setPath(QString::fromStdString(getenv("HOME"))+QString::fromStdString("/Documents"));
    #endif

    return 0;
}

#endif // FUNCTIONS_H
