#include "exportpointers.h"
#include "ui_exportpointers.h"

#include "config.h"

#include <iostream>

//QString default_pointer="00 00 00 00";

exportpointers::exportpointers(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::exportpointers)
{
    ui->setupUi(this);

    setFixedSize(QSize(450, 350));


    absolutePointersFlag=true;
    exportFlag=false;
    excludeFlag=false;

    std::string winTitle="Export pointers";
    winTitle.append(" @ ");
    winTitle.append(PROJECT_NAME);

    setWindowTitle(QString::fromStdString(winTitle));

    setWindowIcon(QIcon(":/icon.ico"));

    ui->btnExportPointers->button(QDialogButtonBox::Save)->setText("Export");
  //  ui->btnExportPointers->button(QDialogButtonBox::Save)->setEnabled(false);
    ui->txtExcludeList->setReadOnly(true);

    ui->chckCustom->setVisible(false);

    home_export_path=getenv("USERPROFILE");
    home_export_path.append("/Documents");
}

exportpointers::~exportpointers()
{
    delete ui;
}

void exportpointers::on_rbtnRelative_toggled(bool checked)
{
    QString p("");
    for (unsigned int i=0;i<pointer_size/2;i++) {
        p.append(bytezero);
        for (unsigned int j=i+1;j<pointer_size/2;j++) {
            p.append(" ");
            break;
        }
    }
    ui->txtFirstPointer->setText(p);
    ui->txtFirstPointer->setEnabled(checked);
    ui->chckCustom->setVisible(checked);
    ui->chckCustom->setEnabled(checked);
    ui->chckCustom->setChecked(!checked);

    absolutePointersFlag=false;
}

void exportpointers::on_rbtnAbs_toggled(bool checked)
{
    ui->txtFirstPointer->setEnabled(!checked);
    ui->chckCustom->setEnabled(!checked);
    ui->chckCustom->setChecked(!checked);

    absolutePointersFlag=true;
}

void exportpointers::on_chckCustom_stateChanged(int arg1)
{
    if(ui->rbtnRelative->isEnabled()){
        ui->txtFirstPointer->setEnabled(!ui->txtFirstPointer->isEnabled());
    }

}

void exportpointers::on_btnExportPointers_accepted()
{

    std::string fpString=ui->txtFirstPointer->text().toStdString();
    std::string::iterator end_pos = std::remove(fpString.begin(), fpString.end(), ' ');
    fpString.erase(end_pos, fpString.end());

   firstPointer=QString::fromStdString(fpString);
   pathExclude=ui->txtExcludeList->toPlainText();
   exportFlag=true;
}



void exportpointers::on_chckExclude_stateChanged(int arg1)
{
    ui->txtExcludeList->setEnabled(!ui->txtExcludeList->isEnabled());
    ui->btnExcludeList->setEnabled(!ui->btnExcludeList->isEnabled());

    excludeFlag=!excludeFlag;
}

void exportpointers::on_btnExcludeList_clicked()
{
    QString selfilter = tr("Text file (*.txt)");
    QString path = QFileDialog::getOpenFileName(
            this,
            "Text file",
            home_export_path,
            tr("Text file (*.txt);; All files (*.*)" ),
            &selfilter
    );

    if(path.isNull()){
        return;
    }

    ui->txtExcludeList->setPlainText(path);
    home_export_path=path;
}

void exportpointers::formatQLineText(QLineEdit *qLine ){

    std::string fpString=qLine->text().toStdString();
    std::string::iterator end_pos = std::remove(fpString.begin(), fpString.end(), ' ');
    fpString.erase(end_pos, fpString.end());

    if(fpString.length()!=pointer_size){
        ui->btnExportPointers->button(QDialogButtonBox::Save)->setEnabled(false);
    }
    else{
        ui->btnExportPointers->button(QDialogButtonBox::Save)->setEnabled(true);
    }
    int s=2;
    std::string noSpaceDigits=fpString;
    for(unsigned int i=s;i<=noSpaceDigits.length();i+=s+1){
        noSpaceDigits.insert(i," ");
    }


    qLine->blockSignals(true);
    if((fpString.length()%2)==0){
        qLine->setText(QString::fromStdString(noSpaceDigits.substr(0,noSpaceDigits.length()-1)));

    }else{
        qLine->setText(QString::fromStdString(noSpaceDigits));

    }

    qLine->setCursorPosition(noSpaceDigits.length());

   qLine->blockSignals(false);
}

void exportpointers::on_txtFirstPointer_textChanged(const QString &arg1)
{
    formatQLineText(ui->txtFirstPointer);
}

void exportpointers::setFirstPointer(QString p){
    ui->txtFirstPointer->setText(p);
}
