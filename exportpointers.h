#ifndef EXPORTPOINTERS_H
#define EXPORTPOINTERS_H

#include <QDialog>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>

using namespace std;

namespace Ui {
class exportpointers;
}

class exportpointers : public QDialog
{
    Q_OBJECT

public:
    explicit exportpointers(QWidget *parent = nullptr);
    ~exportpointers();

    QString firstPointer;
    QString pathExclude;

    bool absolutePointersFlag;

    bool  exportFlag;

    bool excludeFlag;

    QString home_export_path;

    unsigned int pointer_size;

    QString bytezero;

    void setFirstPointer(QString p);

private slots:

    void formatQLineText(QLineEdit *qLine );

    void on_rbtnRelative_toggled(bool checked);

    void on_rbtnAbs_toggled(bool checked);

    void on_chckCustom_stateChanged(int arg1);

    void on_btnExportPointers_accepted();


    void on_chckExclude_stateChanged(int arg1);

    void on_btnExcludeList_clicked();

    void on_txtFirstPointer_textChanged(const QString &arg1);

private:
    Ui::exportpointers *ui;
};

#endif // EXPORTPOINTERS_H
