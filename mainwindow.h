#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "./ui_mainwindow.h"
#include "createproject.h"
#include "exportpointers.h"
#include "inserttranslation.h"
#include "wordconvert.h"
#include "config.h"

#include <QMainWindow>
#include "QListWidgetItem"
#include <QCloseEvent>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QGraphicsView>
#include <QGraphicsScene>

#include <vector>
#include <QString>

#include "nlohmann/json.hpp"

using json=nlohmann::json;
using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool multi_mode;
    bool first_mode;
    bool unordered_mode;
    bool all_mode;
    bool showMessages=true;

private slots:
    int populate_create();

    void setFontSize(Ui::MainWindow *ui,int  size);

    QString intToString(int num);

    wstring readFile(const char* filename);

    QString getLEString(string stringHexIn,bool reverse,bool pointers,int byteSize);

    void writeFile(std::ostream &fileToWrite,QString stringToWrite);

    void uncheckAll();

    void  saveChanges();

    QStringList stringVector_to_QStringList(vector <string> v);

    QString prepareLinesToShow(vector <QStringList> lines,int index);

    QStringList script_entries_to_export(vector < QStringList> script_entries);

    unsigned int msg_check_export_entries();

    int populateGUI( vector <QStringList>eventOriginalLines,vector <QStringList>  eventTranslationLines,vector<bool> entriesWidth, vector<bool> entriesExtraSpace);

    int  prepareTables(vector<string> table);

    void on_actionNew_Project_triggered();

    void on_lstEntries_itemClicked(QListWidgetItem *item);

    unsigned int fromHex(const string &s);

    QString toHex(unsigned int num);

    void controlOptions(bool status);

    void updateElements();

    void setWindowTit(QString basename);

    void closeEvent (QEvent *event);

    QString  getOpen(QString title,QString filter);

    QString getSave(QString title,QString filter,QString name);

    void setCustomFontSize();

    void populateList();

    void set_text_cursor(QTextEdit *text_edit);
    void set_text_cursor_start(QTextEdit *text_edit);

    void convertEndianness(unsigned int byteSize);

    QStringList splitByLines(const QTextDocument *doc);

    vector <string> split(const string& input, const string& regex);

    QMessageBox::StandardButton  confirmCloseProject(QEvent *event);

    void executeCommand(string exec,string parameters);

    QStringList splitByDelimiter(string stringToBeSplitted, string delimeter);

    void closeEvent (QCloseEvent *event);

    void on_action12_triggered();

    void on_action14_triggered();

    void on_action18_triggered();

    void on_action25_triggered();

    void on_action60_triggered();

    void on_actionCustom_triggered();

    void on_btnUpdateTrans_clicked();

    void on_actionSave_Project_triggered();

    void on_actionExport_Text_to_HexDump_triggered();

    void on_actionExport_Pointers_to_HexDump_triggered();

    void on_txtTranslation_textChanged();

    int dumpBinaryToHex(string rawFile, string tempPath);

    int hexDumpToBinary(string hexDump,string tempPath);

    void create_temp_dir();

    void on_actionInsert_Translation_triggered();

    void on_chkFullWidth_stateChanged(int arg1);

    void on_cbFullWidth_currentIndexChanged(int index);

    void on_actionAbout_triggered();

    void on_actionBinary_to_HexDump_triggered();

    void on_actionHexDump_to_Binary_triggered();

    void on_action2_Bytes_triggered();

    void on_action4_Bytes_triggered();

    void on_action1_Byte_triggered();

    void on_action2_Byte_triggered();

    void on_actionAlien_DS_changed();

    void on_chkExtraSpace_stateChanged(int arg1);

    void updateLinesNumber();

    void setTranslationChanged(bool b);

    void setPreview(QStringList str);

    void on_btnBlue_clicked();

    void set_color_words(QString,QTextEdit *text_edit);

    void on_actionExport_Original_Pointers_to_HexDump_triggered();

    void on_actionString_to_Hex_triggered();

    void on_actionLoad_Table_tbl_triggered();

    void setRecentProject();

    void setNewProject(QFileInfo info);

//    void replace_using_sfk(QDir temp_binary_dir,string pointers_original_section_h,string pointers_translation_section_h );

    void init_recent_projects_paths();

    void on_action1_triggered();

    void on_action2_triggered();

    void on_action3_triggered();

    void on_action4_triggered();

    void on_action5_triggered();

    void on_cbWin_currentIndexChanged(int index);

    void updateCharsNumber();

    void on_actionLoad_Module_triggered();

    void on_chkLineBreaks_stateChanged(int arg1);

    void on_btnCopyOriginal_clicked();

    void list_set_color(QListWidgetItem *item, QStringList text);

public slots:

    unsigned int init();

    void findAndReplace(string &data, QString toSearch, QString replaceStr,bool all,bool char_mode);

    void on_actionOpen_Project_triggered(bool recent);

    void insertTranslation(QString file_to_patch_path,QString original_pointers_section_path, QString translation_pointers_path,
                                       QString original_text_section_path,QString translation_text_path,QString out_path,QString original_pointers_path);
    void exportTranslationTextToHex();
    void exportTranslationPointersToHex(vector < QStringList> script_entries);

    void setProject(QFileInfo info);

    void initGlobalVectors(json project);
    void setPaths(QString project,QString table,QString text_export,QString pointers_export,bool pointers_absolute, QString pointers_offset);
    vector < QStringList> getEntriesTranslation();
    vector < QStringList> getEntriesOriginal();

    unsigned int getPointersSize();
    bool isHexString(string str);
    QString getPointersOffset();

    void set_module_file_path(string file_path);
    void set_modules_path(string dir_path);
    void init_editor_module();

private:
    Ui::MainWindow *ui;

    CreateProject *myNewProject;

    exportpointers *myexportpointers;

    InsertTranslation *myInsertTranslation;

   // wordconvert *mywordconvert;


    unsigned int size;

    unsigned int this_row_width;
    unsigned int this_row_extra_space;

};

#endif // MAINWINDOW_H
