#ifndef EVENTS_H
#define EVENTS_H

#include "mainwindow.h"
#include "functions.h"

using namespace std;
using namespace ns;

void MainWindow::on_actionNew_Project_triggered()
{
    myNewProject = new CreateProject; // Be sure to destroy your window somewhere
    myNewProject->exec();
    if(myNewProject->ready){
        QEvent *none=new QEvent(QEvent::None);
        QMessageBox::StandardButton result= confirmCloseProject(none);
        if(result==QMessageBox::Cancel||(result==QMessageBox::Yes&&script_save_path.isNull())){
            return;
        }
        raw_script_path=myNewProject->txt_original_text_section_path;
        table_path=  myNewProject->txt_table_path;
        home_dir.setPath(myNewProject->home_create_path);

//        QFileInfo info(project_dir.path());
//        setProject(info);

        if(!populate_create()){
            controlOptions(true);
            translation_opened=true;
            unsaved_changes=true;
            ui->btnUpdateTrans->setEnabled(false);
            //setRecentProject();
            //home_dir.setPath(raw_script_dir.path());
        }
    }

}

void MainWindow::on_lstEntries_itemClicked(QListWidgetItem *item)
{
    new_row_index= ui->lstEntries->row(item);
    if(translation_changed){
        if(new_row_index==current_row_index){
            ui->lstEntries->setCurrentRow(current_row_index);
        }
        else{
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(this, "Save changes", "Save changes?",
                                          QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
            if (reply == QMessageBox::Yes) {
                saveChanges();
                updateElements();
            } else if(reply == QMessageBox::No) {
                updateElements();
            }
            else if(reply == QMessageBox::Cancel){
                ui->lstEntries->setCurrentRow(current_row_index);
            }
        }

    }
    else{
        updateElements();
        translation_changed=false;
    }

    ui->lstEntries->setCurrentRow(current_row_index);
    item= ui->lstEntries->currentItem();

    try {
        QColor c;
        c.setRgb(0,255,0);
        for (unsigned int i=0;i<(unsigned int)ui->lstEntries->count();i++) {
            list_set_color(ui->lstEntries->item(i),script_entries_translation.at(i));
        }

        c.setRgb(255,255,255);
        item->setBackground(c);

    } catch (exception ex) {
        cout <<"Exception reading color for characters"<<endl;
    }

}

void MainWindow::on_action12_triggered(){
    size=12;
    setFontSize(ui,size);
    uncheckAll();
    ui->action12->setChecked(true);
}
void MainWindow::uncheckAll(){

    ui->action12->setChecked(false);
    ui->action14->setChecked(false);
    ui->action18->setChecked(false);
    ui->action25->setChecked(false);
    ui->action60->setChecked(false);
    ui->actionCustom->setChecked(false);

}

void MainWindow::on_action14_triggered()
{
    size=14;
    setFontSize(ui,size);
    uncheckAll();
    ui->action14->setChecked(true);
}

void MainWindow::on_action18_triggered()
{
    size=18;
    setFontSize(ui,size);
    uncheckAll();
    ui->action18->setChecked(true);
}

void MainWindow::on_action25_triggered()
{
    size=25;
    setFontSize(ui,size);
    uncheckAll();
    ui->action25->setChecked(true);
}

void MainWindow::on_action60_triggered()
{
    size=60;
    setFontSize(ui,size);
    uncheckAll();
    ui->action60->setChecked(true);
}


void MainWindow::on_actionCustom_triggered()
{
    setCustomFontSize();
    uncheckAll();
    ui->actionCustom->setChecked(true);
}

void MainWindow::on_btnUpdateTrans_clicked()
{

    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Save changes", "Save changes?",
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        saveChanges();
        translation_changed=false;
        ui->lstEntries->setCurrentRow(current_row_index);
    }
}

void MainWindow::on_actionSave_Project_triggered()
{
    QDir nameTranslationToSave(project_root_absolute_path+QDir::separator()+project_base_name+
                               project_suffix);
    QString title("Save Translation");
    QString selfilter("Nora Translation (*.nset)");
    script_save_path=getSave(title,selfilter,nameTranslationToSave.path());
    if(script_save_path.isNull()){
        return;
    }
    saveChanges();
    json entries;
    vector <entry> entry;
    for (unsigned int i=0;i<script_entries_original.size();i++) {
        QStringList o=script_entries_original.at(i);
        QStringList t=script_entries_translation.at(i);
        ns::entry e;
        e.original_lines=o;
        e.translation_lines=t;
        e.full_width=script_entries_font_width.at(i);
        e.extra_space=script_entries_extra_space.at(i);
        json this_entry;
        this_entry["extra_space"]=e.extra_space;
        this_entry["full_width"]=e.full_width;
        json original_entry_lines;
        for (unsigned int j=0;j<(unsigned int)e.original_lines.length();j++) {
            original_entry_lines[to_string(j+1)]["text"]=e.original_lines.at(j).toStdString();

        }
        json translation_entry_lines;
        for (unsigned int j=0;j<(unsigned int)e.translation_lines.length();j++) {
                translation_entry_lines[to_string(j+1)]["text"]=e.translation_lines.at(j).toStdString();
        }
        this_entry["original"]=original_entry_lines;
        this_entry["translation"]=translation_entry_lines;
        entries[to_string(i+1)]=this_entry;
    }
    ofstream o(script_save_path.toStdString());
    o<<setw(4)<<entries<<endl;
    o.close();
    //recent_path=script_save_path;
    QFileInfo info(script_save_path);
    project_dir.setPath(info.absoluteFilePath());
    setWindowTit(project_base_name);
    QMessageBox msgBox;
    QString msg("The document has been modified.");
    msgBox.setText(msg);
    msgBox.exec();
    cout <<msg.toStdString()<<endl;
    unsaved_changes=false;
    translation_changed=false;
    ui->lstEntries->setCurrentRow(new_row_index);
    ui->btnUpdateTrans->setEnabled(false);
    setRecentProject();
}

void MainWindow::on_actionOpen_Project_triggered(bool recent)
{
    QString selfilter("Nora Translation (*.nset)");
    QString title("Open Project");
    int args=qApp->arguments().count();
    if(args==2){
        project_dir.setPath(qApp->arguments().at(1));
      //  QFileInfo fileinfo(project_dir.path());
        //project_dir.setPath(fileinfo.absoluteFilePath());
        //setProject(fileinfo());
    }
    else if(!recent){
       project_dir.setPath(getOpen(title,selfilter));
      //  QFileInfo fileinfo(project_dir.path());
      //  setProject(fileinfo());
    }

    if(project_dir.path().isEmpty()){
        return;
    }
    QEvent *none=new QEvent(QEvent::None);
    QMessageBox::StandardButton result= confirmCloseProject(none);
    if(result==QMessageBox::Cancel||(result==QMessageBox::Yes&&script_save_path.isNull())){
        return;
    }
    QFileInfo info(project_dir.path());
    setProject(info);
//    ui->lstEntries->clear();
    ifstream project_stream(project_dir.path().toStdString());
    json project;
    try {
        project_stream >>project;
        initGlobalVectors(project);
        project_stream.close();
    } catch (exception ex) {
        if(showMessages){
            QMessageBox msgBox;
            QString msg("Error loading the project...");
            msgBox.setText(msg);
            msgBox.exec();
            cout <<msg.toStdString()<<endl;
        }
        project_stream.close();
        return;
    }

    if(!populateGUI(script_entries_original,script_entries_translation,script_entries_font_width,script_entries_extra_space)){
        controlOptions(true);
        ui->btnUpdateTrans->setEnabled(false);
        setWindowTit(project_base_name);
        translation_opened=true;
        setRecentProject();
    }

}

void MainWindow::initGlobalVectors(json project){

    script_entries_original.clear();
    script_entries_translation.clear();
    script_entries_font_width.clear();
    script_entries_extra_space.clear();
    for (unsigned int i=0;i<project.size();i++) {
        bool es=project[to_string(i+1)]["extra_space"].get<bool>();
        bool fw=project[to_string(i+1)]["full_width"].get<bool>();
        script_entries_extra_space.push_back(es);
        script_entries_font_width.push_back(fw);
        QStringList lines;
        for (unsigned int j=0;j<project[to_string(i+1)]["original"].size();j++) {
            lines.push_back(QString::fromStdString(project[to_string(i+1)]["original"][to_string(j+1)]["text"].get<std::string>()));
        }
        script_entries_original.push_back(lines);
        lines.clear();
        for (unsigned int j=0;j<project[to_string(i+1)]["translation"].size();j++) {
            if (!project[to_string(i+1)]["translation"][to_string(j+1)]["text"].get<std::string>().length()){
                lines.push_back(QString::fromStdString(project[to_string(i+1)]["original"][to_string(j+1)]["text"].get<std::string>()));
            }
            else{
                lines.push_back(QString::fromStdString(project[to_string(i+1)]["translation"][to_string(j+1)]["text"].get<std::string>()));
            }
        }
        script_entries_translation.push_back(lines);
    }
}

void MainWindow::controlOptions(bool status){
    ui->actionSave_Project->setEnabled(status);
    ui->actionExport_Text_to_HexDump->setEnabled(status);
    ui->actionExport_Pointers_to_HexDump->setEnabled(status);
    ui->actionExport_Original_Pointers_to_HexDump->setEnabled(status);
}

void MainWindow::on_actionExport_Text_to_HexDump_triggered()
{
    exportTranslationTextToHex();
}

void MainWindow::on_actionExport_Pointers_to_HexDump_triggered()
{
    exportTranslationPointersToHex(script_entries_translation);
}

void MainWindow::on_chkFullWidth_stateChanged(int arg1)
{
    ui->cbFullWidth->setEnabled(arg1);
}

void MainWindow::on_cbFullWidth_currentIndexChanged(int index)
{
    this_row_width=ui->cbFullWidth->currentIndex();
    setTranslationChanged(true);
}

void MainWindow::on_actionAbout_triggered()
{
    string text=PROJECT_NAME;
    text.append(" (2023) v");
    text.append(PROJECT_VER);
    QMessageBox msgBox;
    string winTitle="About @ ";
    winTitle.append(PROJECT_NAME);
    msgBox.setWindowTitle(QString::fromStdString(winTitle));
    msgBox.setText(QString::fromStdString(text));
    QPixmap pixmap = QPixmap(":/icon.ico");
    pixmap = pixmap.scaled(45,45);
    msgBox.setIconPixmap(pixmap);
    msgBox.setTextFormat(Qt::RichText);
    text.append("<center style='font-size:13.5px;'><hr/><br/>A script/text translation utility for the NDS platform.<br/><br/>"
                "<a style='color:#D65434;' href='https://gitlab.com/Darukeru/nora-script-editor'>Project</a><br/><br/>"
                "<a style='color:#D65434;' href='https://gitlab.com/Darukeru/nora-script-editor/-/wikis/home'>Wiki</a></center><hr/>"
                "<div style='margin-left:200px;' >Creator: <a style='color:#D65434;' href='https://gbatemp.net/members/darukeru.389565/'>Darukeru</a><div/>");
    msgBox.setText(QString::fromStdString(text));
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.exec();
}

void MainWindow::on_actionBinary_to_HexDump_triggered()
{

    QString filterBinary("Raw binary (*.dat *.ext *.incs *.exe)");
    QString titleBinToHex("Choose a raw binary file");
    QString fileBinToHexPath=getOpen(titleBinToHex,filterBinary);

    if(fileBinToHexPath.isNull()){
        return;
    }

    QFileInfo info(fileBinToHexPath);
    QDir nameBinToHexExport(info.baseName()+hex_prefix+info.suffix());
    QDir pathTempBinToHex(temp_folder_path+QDir::separator()+nameBinToHexExport.path());

    if(dumpBinaryToHex(fileBinToHexPath.toStdString(),pathTempBinToHex.path().toStdString())){
        return;
    }

    QString selfilterHexDump("HexDump (*.dat *.incs *.txt)");
    QString titleSave("Export HexDump");

    QString fileNameExportHex=getSave(titleSave,selfilterHexDump,nameBinToHexExport.path());

    if(fileNameExportHex.isNull()){
        return;
    }

    if (QFile::exists(fileNameExportHex))
    {
        QFile::remove(fileNameExportHex);
    }

    QFile::copy(pathTempBinToHex.path(), fileNameExportHex);

    QMessageBox msgBox;
    QString msg("The hexdump has been exported.");
    msgBox.setText(msg);
    msgBox.exec();
    cout <<msg.toStdString()<<endl;
}

void MainWindow::on_actionHexDump_to_Binary_triggered()
{
    QString filterHexDump("HexDump (*.txt *.dat *.ext *.incs *.exe)");
    QString titleHexToBin("Choose a HexDump file");

    QString fileHexToBinPath=getOpen(titleHexToBin,filterHexDump);

    if(fileHexToBinPath.isNull()){
        return;
    }

    QFileInfo info(fileHexToBinPath);

    QDir nameHexToBinExport(info.baseName()+binary_prefix+info.suffix());

    QDir pathTempBinToHex(temp_folder_path+QDir::separator()+nameHexToBinExport.path());

    if(hexDumpToBinary(fileHexToBinPath.toStdString(),pathTempBinToHex.path().toStdString())){
        return;
    }

    QString selfilterBinary("HexDump (*.exe *.incs *.dat)");
    QString titleSave("Export Binary");

    QString fileNameExportBin=getSave(titleSave,selfilterBinary,nameHexToBinExport.path());

    if(fileNameExportBin.isNull()){
        return;
    }

    if (QFile::exists(fileNameExportBin))
    {
        QFile::remove(fileNameExportBin);
    }

    QFile::copy(pathTempBinToHex.path(), fileNameExportBin);

    QMessageBox msgBox;
    QString msg("The binary has been exported.");
    msgBox.setText(msg);
    msgBox.exec();
    cout <<msg.toStdString()<<endl;
}

void MainWindow::on_action2_Bytes_triggered()
{
    convertEndianness(2);

}

void MainWindow::on_action4_Bytes_triggered()
{
    convertEndianness(4);
}

void MainWindow::on_actionInsert_Translation_triggered()
{
    //Create new instance of a new Insert Translation dialog window
    myInsertTranslation = new InsertTranslation;

    //Wait for the Insert Translation window to be terminated
    myInsertTranslation->exec();

    //Get the flags specified by the user
    bool insertFlag=myInsertTranslation->insertFlag;
    multi_mode=myInsertTranslation->modeMulti;
    first_mode=myInsertTranslation->modeFirst;
    unordered_mode=myInsertTranslation->modeUnordered;
    home_dir.setPath(myInsertTranslation->home_insert_path);

    //If no valid paramters, stop
    if(!insertFlag){
        return;
    }

    //Call the insert translation function passing the parameters from the insert dialog object
    insertTranslation(myInsertTranslation->filePatchPath,myInsertTranslation->pointersOriginalSectionPath,
                      myInsertTranslation->pointersTranslationPath,myInsertTranslation->textOriginalSectionPath,
                      myInsertTranslation->textTranslationPath,"",myInsertTranslation->pointersOriginal);


}


void MainWindow::on_action1_Byte_triggered()
{
    entry_delimiter="00"; // 4 zeros indicate end of an entry
    entry_delimiter_custom="7C"; // Character |
    char_size=2; //2 bytes
    //sc_size=6;
    s_A_half="5B415D"; //[A] special code half size
    s_A_full="3B213D"; //[A] special code full size
    s_A="0A";

    ui->action2_Byte->blockSignals(true);
    ui->action2_Byte->setChecked(false);
    ui->action2_Byte->blockSignals(false);
}

void MainWindow::on_action2_Byte_triggered()
{
    entry_delimiter="0000"; // 4 zeros indicate end of an entry
    entry_delimiter_custom="007C"; // Character |
    char_size=4; //2 bytes
    //sc_size=12;
    s_A_half="5B0041005D00"; //[A] special code half size
    s_A_full="3BFF21FF3DFF"; //[A] special code full size
    s_A="0A00";

    ui->action1_Byte->blockSignals(true);
    ui->action1_Byte->setChecked(false);
    ui->action1_Byte->blockSignals(false);

}

void MainWindow::on_actionAlien_DS_changed()
{

    special_settings=ui->actionAlien_DS->isChecked();
}


void MainWindow::on_chkExtraSpace_stateChanged(int arg1)
{
    if(arg1){
        ui->txtTranslation->setMinimumWidth(extra_space_width);
    }
    else{
        ui->txtTranslation->setMinimumWidth(normal_space_width);
    }

    ui->chkExtraSpace->setChecked(arg1);

    updateLinesNumber();

    this_row_extra_space=arg1;
    setTranslationChanged(true);
}

void MainWindow::on_btnBlue_clicked()
{

    QString spanPre="<span style=\"color:blue\">";
    QString spanPost="</span>";

    QString sel=ui->txtTranslation->textCursor().selectedText();

    ui->txtTranslation->textCursor().insertHtml(sc_blue_words_delimiters.at(0)+spanPre+sel+spanPost+sc_blue_words_delimiters.at(1));
}




#endif // EVENTS_H
