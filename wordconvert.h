#ifndef WORDCONVERT_H
#define WORDCONVERT_H

#include <QDialog>
#include <QString>
#include <QAbstractButton>

#include <iostream>

using namespace std;

namespace Ui {
class wordconvert;
}

class wordconvert : public QDialog
{
    Q_OBJECT

public:
    explicit wordconvert(QWidget *parent = nullptr);
    ~wordconvert();

    QString word;
    QString hex;

    bool table_loaded;

    vector<bool> script_entries_font_width;

    unsigned int entry_width;

    QStringList spanishCharacters;

    vector<string>  table_codes,table_characters;

    string stringToHex(QStringList script_entries_export);


private slots:

    void on_btnConvertWord_clicked(QAbstractButton *button);

    void on_pushButton_clicked();

private:
    Ui::wordconvert *ui;
};

#endif // WORDCONVERT_H
