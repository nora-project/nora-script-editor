#include "mainwindow.h"
#include <iomanip>

#include <QApplication>

using namespace std;

void error_params(){
    cout <<"\n\tWrong arguments!";
    cout <<"\n\n\t##############################################################\n\n\tTO EXPORT:"
           "\n\n\t./NoraScriptEditor --export -nset-file-path -tbl-path -translation-text-hex-path -translation-pointers-hex-path -mode[--absolute,--relative] -first-pointer option[-noMsgs] --module -module-nsem-path "
           "\n\n\tExample:\n\n\t./NoraScriptEditor --export ./MenuText.nset ./table.tbl ./translation-text-hex/MenuText.txt ./translation-pointers-hex/MenuText.txt --absolute 000012A0 --noMsgs --module ./modules/default.nsem "
           "\n\n\t##############################################################\n\n\tTO INSERT:"
           "\n\n\t./NoraScriptEditor --insert -raw-file-path -original-pointers-hex-path -translation-pointers-hex-path -original-text-hex-path  -translation-text-hex-path -patched-output-path -mode[--multi,--unordered,--first,--all] -option[--noMsgs] -option[-cleaned-original-pointers-hex-path] "
           "\n\n\tExample:\n\n\t./NoraScriptEditor --insert MenuText.data ./original-pointers-hex/MenuText.txt ./translation-pointers-hex/MenuText.txt ./original-text-hex/MenuText.txt  ./translation-text-hex/MenuText.txt  ./patched-output/MenuText.data --multi --noMsgs ./clean-pointers-hex/MenuText.txt"<<endl;

}

int main(int argc, char** argv)
{

    QApplication a(argc, argv);

    MainWindow *w=new MainWindow();


    //Read config.ini
    if(w->init()){
        a.quit();
        return 1;
    }

    //cout <<"ARGC: "<<argc<<endl;
    if(argc==1){
        goto no_args;
    }

    //Check if command line mode

    //Export translation pointers and text, command line mode
    if(!strcmp(argv[1],"--export")){
        if(argc!=10&&argc!=11){
            error_params();
            a.quit();
            return 1;
        }
        //Read paths
        string project_path=argv[2];
        string table_path=argv[3];
        string text_export_path=argv[4];
        string pointers_export_path=argv[5];
        //Set pointers type
        bool pointers_absolute;
        if(!strcmp(argv[6],"--absolute")){
            pointers_absolute=true;
        }
        else if (!strcmp(argv[6],"--relative")){
            pointers_absolute=false;
        }
        else{
            cout <<"Wrong pointers type"<<endl;
            a.quit();
            return 1;
        }

        //Default pointers offset
        string pointers_offset=w->getPointersOffset().toStdString();
        if (argc==11){
            string arg = argv[7];
            //If argument is a hex string and the size of a pointer
            if(w->isHexString({arg.begin(),arg.end()})&&arg.size()==w->getPointersSize()){
                pointers_offset=argv[7];
            }
            else{
                cout <<"Wrong pointers offset"<<endl;
                a.quit();
                return 1;
            }
            if(!strcmp(argv[8],"--noMsgs")){
                w->showMessages=false;
            }
            else{
                cout <<"Wrong argument"<<endl;
                a.quit();
                return 1;
            }
        }

        else{

            string arg = argv[7];
            //If argument is a hex string and the size of a pointer
            if(w->isHexString({arg.begin(),arg.end()})&&arg.size()==w->getPointersSize()){
                pointers_offset=argv[7];
            }
            else if(arg=="--noMsgs"){
                w->showMessages=false;
            }
            else{
                cout <<"Wrong argument!"<<endl;
                a.quit();
                return 1;
            }
        }

        //Module file
        string module_file_path;
        if(!strcmp(argv[argc-2],"--module")){
            module_file_path=argv[argc-1];
            w->set_module_file_path(module_file_path);
        }

        //Load module
        w->init_editor_module();

        //Set global current project
        QFileInfo info(QString::fromStdString(project_path));
        w->setProject(info);
        //Set global current paths
        w->setPaths(QString::fromStdString(project_path),
                   QString::fromStdString(table_path),
                   QString::fromStdString(text_export_path),
                   QString::fromStdString(pointers_export_path),
                   pointers_absolute,
                   QString::fromStdString(pointers_offset));
        //Read json project
        ifstream project_stream(project_path);
        json project;
        try {
            project_stream>>project;
            project_stream.close();
        } catch (exception ex) {
            if(w->showMessages){
                QMessageBox msgBox;
                QString msg("Failed to load the project...");
                msgBox.setText(msg);
                cout <<msg.toStdString()<<endl;
            }
            project_stream.close();
            return 1;
        }
        //Populate global dialogue vectors
        w->initGlobalVectors(project);
        //Export translation text to an insertable hexdump
        w->exportTranslationTextToHex();
        //Export translation pointers to an insertable hexdump
        w->exportTranslationPointersToHex(w->getEntriesTranslation());

        if(w->showMessages){
            QMessageBox msgBox;
            QString msg("Exported successfully!");
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
        }

        a.quit();
        return 0;
    }

    if(!strcmp(argv[1],"--insert")){
        if(argc!=9&&argc!=10&&argc!=11){
            error_params();
            a.quit();
            return 1;
        }

        //Iterates over the pointers string every X characters, depending the pointer size
        //Replaces all the pointer coincidences
        if(!strcmp(argv[8],"--multi")){
            w->multi_mode=true;
            w->unordered_mode=false;
            w->first_mode=false;
            w->all_mode=false;
        }
        else if(!strcmp(argv[8],"--unordered")){
            w->multi_mode=false;
            w->unordered_mode=true;
            w->first_mode=false;
            w->all_mode=false;
        }
        //Iterates over the pointers string every X characters, depending the pointer size
        //Replaces only the first pointer coincidence
        else if(!strcmp(argv[8],"--first")){
            w->multi_mode=false;
            w->unordered_mode=false;
            w->first_mode=true;
            w->all_mode=false;
        }
        //Replaces the whole pointers section by the new pointers, in a single commmand
        else if(!strcmp(argv[8],"--all")){
            w->multi_mode=false;
            w->unordered_mode=false;
            w->first_mode=false;
            w->all_mode=true;
        }
        else{
            a.quit();
            cout<<"Wrong mode!"<<endl;
            return -1;
        }

        string raw_file_path=argv[2];
        string pointers_original_section_path=argv[3];
        string pointers_translation_path=argv[4];
        string text_original_section_path=argv[5];
        string text_translation_path=argv[6];
        string out_path=argv[7];

        string pointers_original_path;

        if(argc==10){
            if(!strcmp(argv[9],"--noMsgs")){
                w->showMessages=false;
            }
            else{
                if(w->first_mode||w->multi_mode){
                    pointers_original_path=argv[9];
                }
                else{
                    a.quit();
                    return -1;
                }
            }
        }
        else if(argc==11){
            if(!strcmp(argv[9],"--noMsgs")){
                w->showMessages=false;
            }
            else{
                a.quit();
                return -1;
            }

            if(w->first_mode||w->multi_mode||w->unordered_mode){
                pointers_original_path=argv[10];
            }
            else{
                a.quit();
                error_params();
                return -1;
            }

        }

        w->insertTranslation(QString::fromStdString(raw_file_path),
                            QString::fromStdString(pointers_original_section_path),
                            QString::fromStdString(pointers_translation_path),
                           QString::fromStdString(text_original_section_path),
                            QString::fromStdString(text_translation_path),
                            QString::fromStdString(out_path),
                            QString::fromStdString(pointers_original_path));

        if(w->showMessages){
            QMessageBox msgBox;
            QString msg("Inserted successfully!");
            msgBox.setText(msg);
            cout <<msg.toStdString()<<endl;
        }
        a.quit();
        return 0;
    }

    no_args:
    //Load default module
    w->init_editor_module();

    //If GUI mode
    w->show();

    //Check if double click on associated translation project
    if(argc==2){
       w->on_actionOpen_Project_triggered(false);
    }

    return a.exec();
}
