#ifndef INSERTTRANSLATION_H
#define INSERTTRANSLATION_H

#include <QFileDialog>
#include <string>

#include <fstream>
#include <iostream>

using namespace std;

namespace Ui {
class InsertTranslation;
}

class InsertTranslation : public QDialog
{
    Q_OBJECT

public:
    explicit InsertTranslation(QWidget *parent = nullptr);
    ~InsertTranslation();

    QString getOpen(QString title,QString filter);

    bool insertFlag,modeMulti,modeUnordered,modeFirst, modeAll;

    QString filePatchPath;
    QString pointersOriginalSectionPath;
    QString pointersTranslationPath;
    QString textOriginalSectionPath;
    QString textTranslationPath;

    QString pointersOriginal;

    QString home_insert_path;


private slots:

    void checkInputs();

    void on_btnFilePatch_clicked();

    void on_btnPointersTranslation_clicked();

    void on_btnTextTranslation_clicked();

    void on_txtFilePatch_textChanged();

    void on_txtPointersTranslation_textChanged();

    void on_txtTextTranslation_textChanged();

    void on_btnInsertTranslation_accepted();

    void on_InsertTranslation_rejected();

    void on_btnInsertTranslation_rejected();

    void on_chkMulti_toggled(bool checked);

    void on_chkFirst_toggled(bool checked);

    void on_chkAll_toggled(bool checked);

    void on_btnTextOriginalSection_clicked();

    void on_btnPointersOriginalSection_clicked();

    void on_btnPointersOriginal_clicked();

    void on_txtPointersOriginalSection_textChanged();

    void on_txtTextOriginalSection_textChanged();

    void on_txtPointersOriginal_textChanged();

    void on_chkUnordered_toggled(bool checked);

private:

    Ui::InsertTranslation *ui;

};

#endif // INSERTTRANSLATION_H
