# Nora Script Editor

**Nora Script Editor** has been originally created to easily translate, save and modify the NDS game _Nora to Toki no Koubou: Kiri no Mori no Majo_ text scripts. 

***This software is still in constant development.***

----
Usage
---
**GUI functionalities**

* Create a new script translation

* Save a current translation 

* Generate an insertable translation 

* Generate the translation pointers

* Insert the translation into the original file

**Command line functionalities**

* Insert the translation into the original file



**Visit the [Wiki](https://gitlab.com/Darukeru/nora-script-editor/-/wikis/home) for a more detailed explanation**

Compiling
---
_Nora SE_ is a Qt6 GUI application, in order to build it you'd need to have a working installation of this framework. You can find more information on https://doc.qt.io/qt-6/gettingstarted.html.

To make use of some of the software functionalities you need a recent version of the **Swiss File Knife** command line utility properly configured and accessible in your command line. More information on its official website http://stahlworks.com/dev/swiss-file-knife.html.

Licensing
---
This software is licensed under the terms of the GPLv3.  You can access to a copy of this license in the License file.

# Credits

* Swiss File Knife https://sourceforge.net/projects/swissfileknife/