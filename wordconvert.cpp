#include "wordconvert.h"
#include "ui_wordconvert.h"

#include "config.h"

#include "QMessageBox"
#include "QClipboard"

wordconvert::wordconvert(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::wordconvert)
{
    ui->setupUi(this);
}

wordconvert::~wordconvert()
{
    delete ui;
}


string wordconvert::stringToHex(QStringList script_entries_export){
    string translation_hex("");
    if(!table_loaded){
        QMessageBox msgBox;
        QString msg("No table loaded!");
        msgBox.setText(msg);
        msgBox.exec();
        cout <<msg.toStdString()<<endl;
        return translation_hex;
    }

    // The \n line breaks are replaced automatically
    //Convert the translation to hex dump code, including end entry code 0000
    for (unsigned int i=0;i<(unsigned int)script_entries_export.size();i++) {
        string this_entry=script_entries_export.at(i).toStdString();
        for (size_t  j=0;j<this_entry.length();) {
            int cplen = 1;
            if((this_entry[j] & 0xf8) == 0xf0) cplen = 4;
            else if((this_entry[j] & 0xf0) == 0xe0) cplen = 3;
            else if((this_entry[j] & 0xe0) == 0xc0) cplen = 2;
            if((j + cplen) > this_entry.length()) cplen = 1;
            string this_char=this_entry.substr(j,cplen);
            j += cplen;

            //Check if spanish characters

            bool spanishCharacter=false;

            for (unsigned int k=0;k<(unsigned int)spanishCharacters.size();k++) {
                if(spanishCharacters.at(k).toStdString()==this_char){
                    spanishCharacter=true;
                    break;
                }
            }

            //Start text conversion
            for (unsigned k=0;k<table_characters.size();k++) {
                if(this_char==table_characters.at(k)){
                    if(script_entries_font_width.at(i)&&!spanishCharacter){
                        translation_hex.append(table_codes.at(k+entry_width));
                    }
                    else{
                        translation_hex.append(table_codes.at(k));
                    }
                    break;
                }
            }

        }
    }
    return translation_hex;
}

void wordconvert::on_btnConvertWord_clicked(QAbstractButton *button)
{
    word=ui->txtWord->toPlainText();

    QStringList words;
    words.push_back(word);

    for (unsigned int i=0;i<(unsigned int)words.size();i++) {
        script_entries_font_width.push_back(false);
    }
    string s=stringToHex(words);
    ui->txtHex->setPlainText(QString::fromStdString(s));
}

void wordconvert::on_pushButton_clicked()
{
    QString hex(ui->txtHex->toPlainText());

    QClipboard *p_Clipboard = QApplication::clipboard();
    p_Clipboard->setText(hex);
    QMessageBox msgBox;
    QString msg("Copied to clipboard");
    msgBox.setText(msg);
    cout <<msg.toStdString()<<endl;
    msgBox.exec();
}
