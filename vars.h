#ifndef VARS_H
#define VARS_H

#include <QFileDialog>
#include <QListWidgetItem>
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>
#include <QCloseEvent>
#include <QEvent>
#include <QTextBlock>
#include <QTextStream>
#include <QSettings>

#include <fstream>
#include <vector>
#include <regex>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <codecvt>
#include <string>
#include <fcntl.h>
#include <math.h>
#include <locale>
#include "nlohmann/json.hpp"

#ifdef _WIN32
        //#include <sys/io.h>
        #include <windows.h>
        #include <ShellAPI.h>
#else
        //#include <sys/uio.h>
        #include <cstdlib>
#endif

#include "Entry.cpp"

using json=nlohmann::json;

json fonts,win_imgs;
//vector <unsigned int> png_font_positionsy;
unsigned int max_lines,
font_space_offset,
font_width,
pointer_size, //8 letters
char_size, //2 bytes
//sc_size, //12 letters (3 characters)
untranslated=0,
font_size_default,
list_font_size_default,
extra_space_width,
normal_space_width,
entry_width,
current_row_index,
new_row_index,
sizetwo, // 2-byte (4 characters)
sizeone, // 1-byte (2 characters)
space_char_w,
ch_s;

QString sc_alien, // %a%
sc_alien_hex,
entry_delimiter, // 4 zeros indicate end of an entry
entry_delimiter_custom, // Character |
s_A_half, //[A] special code half size
s_A_full, //[A] special code full size
s_A, //Special code
ch_line_break_escape="\\n",
ch_line_break="\n",
ch_equal_escape="\\=",
ch_comma_escape="\\,",
prefix_latin_char_hex,
ch_line_break_html="<br/>",
//script_path,
table_path,
last_used_path,
script_save_path,
raw_script_path,
project_base_name,
project_suffix,
project_filename,
project_root_absolute_path,
res_path,
fonts_path,
text_window_path,
recent_project_path,
temp_folder_path,
temp_copy_suffix,
script_translation_export_suffix,
script_pointers_export_suffix,
binary_prefix,
hex_prefix,
//project_suffix,
converted_prefix,
byte_prefix,
patched_prefix,
patched_binary_prefix,
//delimiter_original,
//delimiter_translation,
//delimiter_width,
full_latin1, //¿
full_latin2, //¡
codeFirst,
codeScnd,
codeThrd,
/*
codeFullReplace,
codeFullReplace2,
*/
ccode1,
ccode2,
fonts_table_path,
win_dir_path,
win_json_path,
modules_path,
module_file_path("default.nsem"),
//project_path,
text_export_path,
pointers_export_path,
pointers_offset,
w_title,
bytezero="00",
config_ini_file="config.ini";

QStringList
//es_chars,
lat_chars_h_codes,
lat_chars,
recent_list;

vector<QString> sc_blue_words_delimiters;
vector < QStringList> script_entries_original,script_entries_translation;
vector<string>  table_codes,table_characters;
vector<bool> script_entries_font_width, script_entries_extra_space;

bool reverse_flag=false,
translation_changed,
translation_opened,
unsaved_changes=false,
new_populate=false,
special_settings=false,
table_loaded=false,
pointers_absolute,
by_lines=true;

//const char *open_script_full_path;

QDir project_dir,
raw_script_dir,
home_dir;

#endif // VARS_H
