#ifndef CREATEPROJECT_H
#define CREATEPROJECT_H


#include <QFileDialog>
#include <QPushButton>

using namespace std;


namespace Ui {
class CreateProject;
}

class CreateProject : public QDialog
{
    Q_OBJECT

public:
    explicit CreateProject(QWidget *parent = nullptr);
    ~CreateProject();

    QString txt_table_path;
    QString txt_original_text_section_path;
    QString home_create_path;

    bool ready=false;



private slots:
    void on_btnChooseTextDump_clicked();

    void on_btnChooseTable_clicked();

    void on_btnGenerateNewProject_accepted();

    void checkReadyPaths();

    void on_CreateProject_rejected();

    QString getOpen(string title,string filter);

private:
    Ui::CreateProject *ui;
};

#endif // CREATEPROJECT_H
