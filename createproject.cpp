#include "createproject.h"
#include "./ui_createproject.h"

#include "config.h"

CreateProject::CreateProject(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateProject)
{
    ui->setupUi(this);

    setFixedSize(QSize(400, 300));


    string winTitle="Create project";
    winTitle.append(" @ ");
    winTitle.append(PROJECT_NAME);


    setWindowTitle(QString::fromStdString(winTitle));

    setWindowIcon(QIcon(":/icon.ico"));

    ui->btnGenerateNewProject->button(QDialogButtonBox::Save)->setText("Create");

    ready=false;

    ui->btnGenerateNewProject->button(QDialogButtonBox::Save)->setDisabled(true);

    home_create_path=getenv("USERPROFILE");
    home_create_path.append("/Documents");
}



CreateProject::~CreateProject()
{
    delete ui;
}

void CreateProject::on_btnChooseTextDump_clicked()
{

    string filterHex="Nora HexDump (*.dat *.bin *.txt *.incs *.loctext)";
    string titleOpen="Open HexDump";

    QString fileName=getOpen(titleOpen,filterHex);

    ui->txtLblPathText->setPlainText(fileName);
    checkReadyPaths();
}

void CreateProject::on_btnChooseTable_clicked()
{

    string filterTable="Table (*.tbl *.txt)";
    string titleTable="Open table";

    QString fileName=getOpen(titleTable,filterTable);


    ui->txtLblPathTable->setPlainText(fileName);
    checkReadyPaths();
}

void CreateProject::on_btnGenerateNewProject_accepted()
{
    txt_original_text_section_path=ui->txtLblPathText->toPlainText();
    txt_table_path=ui->txtLblPathTable->toPlainText();
    ready=true;

}

void CreateProject::checkReadyPaths(){

    QString bte=ui->txtLblPathText->toPlainText();
     QString bta=ui->txtLblPathTable->toPlainText();


    if(bte!=""&&bta!=""){
        ui->btnGenerateNewProject->button(QDialogButtonBox::Save)->setDisabled(false);
    }
    else{
        ui->btnGenerateNewProject->button(QDialogButtonBox::Save)->setDisabled(true);
    }
}

void CreateProject::on_CreateProject_rejected()
{
    ready=false;
}

QString CreateProject::getOpen(string title,string filter){
    string myFilter=filter;

    string myAllFilters=myFilter;
    myAllFilters.append(";; All files (*.*)");

    QString selFilter = QString::fromStdString(filter);
    QString path = QFileDialog::getOpenFileName(
                this,
                QString::fromStdString(title),
                QString::fromStdString(home_create_path.toStdString()),
                QString::fromStdString(myAllFilters),
                &selFilter
                );

    home_create_path=path;
    return path;
}
