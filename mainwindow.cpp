#include "mainwindow.h"
#include "events.h"
#include "QSettings"

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(QSize(1340, 700));
    project_base_name=QString("untitled");
    w_title=" @ ";
    w_title.append(PROJECT_NAME);
    last_used_path="";
    unsaved_changes=false;
    translation_opened=false;
    this_row_width=0;
    reverse_flag=false;
    translation_changed=false;
    script_entries_original.clear();
    script_entries_translation.clear();
    current_row_index=0;
    new_row_index=0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExport_Original_Pointers_to_HexDump_triggered()
{
    exportTranslationPointersToHex(script_entries_original);
}

void MainWindow::on_actionString_to_Hex_triggered()
{
    wordconvert *mywordconvert = new wordconvert; // Be sure to destroy your window somewhere
    mywordconvert->table_codes=table_codes;
    mywordconvert->table_characters=table_characters;
    mywordconvert->table_loaded=table_loaded;
    mywordconvert->entry_width=entry_width;
    mywordconvert->spanishCharacters=lat_chars;
    mywordconvert->exec();
}

void MainWindow::on_actionLoad_Table_tbl_triggered()
{
    QString table_filter("Table (*.tbl *.txt)");
    QString table_title("Open table");
    QString table_dir=getOpen(table_title,table_filter);

    if(table_dir.isEmpty()){

        if(showMessages){
            QMessageBox msgBox;
            QString msg("No table loaded!");
            msgBox.setText(msg);
            msgBox.exec();
            cout <<msg.toStdString()<<endl;
        }
        return;
    }

    last_used_path = QFileInfo(table_dir).path();
    //Read the table characters
    ifstream tableStream {table_dir.toStdString() };
    string str;
    vector<string> tableList;
    while (getline(tableStream, str))
    {
        tableList.push_back(str);
    }

    //Prepare the table codes and characters list
    if(prepareTables(tableList)){
        return;
    }

    if(showMessages){
        QMessageBox msgBox;
        QString msg("Table loaded!");
        msgBox.setText(msg);
        msgBox.exec();
        cout <<msg.toStdString()<<endl;
    }

}

void MainWindow::setPaths(QString project,QString table,QString text_export,QString pointers_export,bool pointers_abs, QString pointers_off){
    project_dir.setPath(project);
    table_path=table;
    pointers_export_path=pointers_export;
    text_export_path=text_export;
    pointers_absolute=pointers_abs;
    pointers_offset=pointers_off;
}

vector < QStringList> MainWindow::getEntriesTranslation(){
    return script_entries_translation;
};
vector < QStringList> MainWindow::getEntriesOriginal(){
    return script_entries_original;
};

unsigned int MainWindow::getPointersSize(){
    return pointer_size;
}

void MainWindow::set_modules_path(string dir_path){
    modules_path=QString::fromStdString(dir_path);
}

void MainWindow::set_module_file_path(string file_path){
    module_file_path=QString::fromStdString(file_path);
}

bool MainWindow::isHexString(string str){

    if(all_of(str.begin(),str.end(),::isxdigit)){
        return true;
    }
    return false;
}

QString MainWindow::getPointersOffset(){
    return pointers_offset;
}

void MainWindow::init_recent_projects_paths(){
    QDir dir_config(QCoreApplication::applicationDirPath()+QDir::separator()+config_ini_file);
    QSettings settings(dir_config.absolutePath(),QSettings::IniFormat);
    settings.sync();
    settings.beginGroup("recent");
    QList <QAction*> l(ui->menuOpen_Recent_Project->actions());
    unsigned int begin,max=30;
    recent_list.clear();
    for (unsigned int i=0;i<(unsigned int)l.size();i++) {
        QString r(settings.value(QString::fromStdString(to_string(i+1))).toString());
        begin=0;
        if(r.toStdString().length()>max){
            begin=r.toStdString().length()-max;
        }
        string name=r.toStdString().substr(begin,r.toStdString().length());
        l.at(i)->setText(QString::fromStdString(name));
        l.at(i)->setData(r);
        l.at(i)->setToolTip(r);
        if(r.isEmpty()){
            l.at(i)->setEnabled(false);
        }
        recent_list.push_back(r);
    }
    settings.sync();
    settings.endGroup();
}

void MainWindow::setRecentProject(){

    if(recent_project_path==project_dir.path()){
        return;
    }
    recent_project_path=project_dir.path();

    QDir dir_config(QCoreApplication::applicationDirPath()+QDir::separator()+config_ini_file);
    QSettings settings(dir_config.absolutePath(),QSettings::IniFormat);
    settings.sync();
    settings.beginGroup("recent");
    QList <QAction*> l(ui->menuOpen_Recent_Project->actions());

    bool removed=false;
    for (unsigned int i=0;i<(unsigned int)recent_list.size();i++) {
        if(recent_project_path==recent_list.at(i)){
            recent_list.removeAt(i);
            removed=true;
            break;
        }
    }
    if(!removed){
        recent_list.removeAt(recent_list.size()-1);
    }
    recent_list.push_front(recent_project_path);

    unsigned int begin,max=30;
    for (int i=0;i<recent_list.size();i++) {
        settings.setValue(QString::fromStdString(to_string(i+1)),recent_list.at(i));

        begin=0;
        if(recent_list.at(i).toStdString().length()>max){
            begin=recent_list.at(i).toStdString().length()-max;
        }
        string name=recent_list.at(i).toStdString().substr(begin,recent_list.at(i).toStdString().length());
        l.at(i)->setText(QString::fromStdString(name));
        l.at(i)->setData(recent_list.at(i));
        if(recent_list.at(i).isEmpty()){
            l.at(i)->setEnabled(false);
        }
        else{
            l.at(i)->setEnabled(true);
        }
    }
    settings.sync();
    settings.endGroup();
}

void MainWindow::on_action1_triggered()
{
    project_dir.setPath(ui->action1->data().toString());
    on_actionOpen_Project_triggered(true);
}

void MainWindow::on_action2_triggered()
{
    project_dir.setPath(ui->action2->data().toString());
    on_actionOpen_Project_triggered(true);
}

void MainWindow::on_action3_triggered()
{
    project_dir.setPath(ui->action3->data().toString());
    on_actionOpen_Project_triggered(true);
}

void MainWindow::on_action4_triggered()
{
    project_dir.setPath(ui->action4->data().toString());
    on_actionOpen_Project_triggered(true);
}

void MainWindow::on_action5_triggered()
{
    project_dir.setPath(ui->action5->data().toString());
    on_actionOpen_Project_triggered(true);
}

void MainWindow::on_cbWin_currentIndexChanged(int index)
{
    try {
        setPreview(script_entries_translation.at(current_row_index));
    } catch (exception ex) {

    }
}

void MainWindow::on_actionLoad_Module_triggered()
{
    QString module_filter("Nora Module (*.nsem *.txt)");
    QString module_title("Load Nora Module");
    QString module_dir=getOpen(module_title,module_filter);

    QFileInfo info(module_dir);

    QString dir,file;
    dir=info.absolutePath();
    file=info.baseName()+"."+info.suffix();
    modules_path=dir;
    module_file_path=file;

    if(showMessages){
        QString msg("Module loaded!");
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.exec();
        cout <<msg.toStdString()<<endl;
    }

    set_module_file_path(file.toStdString());
    set_modules_path(dir.toStdString());
    init_editor_module();

}

void MainWindow::on_chkLineBreaks_stateChanged(int arg1)
{
    by_lines=!arg1;
    ui->chkLineBreaks->setChecked(arg1);
    setTranslationChanged(true);
}

void MainWindow::on_btnCopyOriginal_clicked()
{
    ui->txtTranslation->setText(ui->txtOriginal->toPlainText());
}
