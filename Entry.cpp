#include <iostream>

#include <QStringList>

using namespace std;

    namespace ns {


    struct entry{
        QStringList translation_lines;
        QStringList original_lines;
        bool full_width;
        bool extra_space;
    };

    }
