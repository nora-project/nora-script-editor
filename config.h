#ifndef INCLUDE_GUARD
#define INCLUDE_GUARD

#define PROJECT_NAME "Nora Script Editor"
#define PROJECT_VER  "0.7.3"
#define PROJECT_VER_MAJOR "0"
#define PROJECT_VER_MINOR "7"
#define PTOJECT_VER_PATCH "3"

#endif // INCLUDE_GUARD
